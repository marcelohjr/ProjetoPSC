package DomainModel;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "notaFiscal")

public class NotaFiscal {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codigo;
    @Column
    private String descricaoFornecedor;
    @Column
    private int idFornecedor;
    @Column
    private String cnpj;
    @Column
    private String cpf;
    @Column
    private long numero;
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataEmissao;
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataEntrega;
    @Column
    private float valorTotal;
    @Column
    private float acrescimos;
    @Column
    private float descontos;
    
    public NotaFiscal(){}

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricaoFornecedor() {
        return descricaoFornecedor;
    }

    public void setDescricaoFornecedor(String descricaoFornecedor) {
        this.descricaoFornecedor = descricaoFornecedor;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public Date getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public Date getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }

    public float getAcrescimos() {
        return acrescimos;
    }

    public void setAcrescimos(float acrescimos) {
        this.acrescimos = acrescimos;
    }

    public float getDescontos() {
        return descontos;
    }

    public void setDescontos(float descontos) {
        this.descontos = descontos;
    }  

    public int getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}