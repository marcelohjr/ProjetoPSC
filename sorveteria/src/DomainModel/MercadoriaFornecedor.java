package DomainModel;
import javax.persistence.*;

@Entity
@Table(name = "mercadoriaFornecedor")

public class MercadoriaFornecedor {
    @Id
    private int idMercadoria;
    @Id
    private int idFornecedor;
    
    public MercadoriaFornecedor(){}

    public int getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public int getIdMercadoria() {
        return idMercadoria;
    }

    public void setIdMercadoria(int idMercadoria) {
        this.idMercadoria = idMercadoria;
    }
}
