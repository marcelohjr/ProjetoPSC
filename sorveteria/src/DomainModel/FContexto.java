package DomainModel;

import Apresentação.JFTelaLogin;
import Apresentação.JFTelaInicial;
import Apresentação.JFTelaNotaFiscal;
import Apresentação.ListarNotasFiscais;
import Apresentação.JFTelaUsuarios;
import Apresentação.ListarUsuarios;
import Apresentação.JFTelaFornecedores;
import Apresentação.ListarFornecedores;
import Apresentação.JFTelaMercadorias;
import Apresentação.ListarMercadorias;
import Apresentação.JFTelaCaixaAbrir;
import Apresentação.JFTelaCaixaFechar;
import Apresentação.JFTelaVendas;
import Apresentação.JFTelaRetiradas;
import Apresentação.JFTelaRelatorios;
import Apresentação.JFRelatorioEstoque;
import Apresentação.JFRelatorioVendas;
import Apresentação.JFRelatorioFinanceiro;

public class FContexto {
    public static void main(String[] args) {
        FContexto.getLogin().setVisible(true);
    }
    
    private static JFTelaLogin jFTelaLogin;
    private static JFTelaInicial jFTelaInicial;
    private static JFTelaNotaFiscal jFTelaNotaFiscal;
    private static ListarNotasFiscais listarNotasFiscais;
    private static JFTelaUsuarios jFTelaUsuarios;
    private static ListarUsuarios listarUsuarios;
    private static JFTelaFornecedores jFTelaFornecedores;
    private static ListarFornecedores listarFornecedores;
    private static JFTelaMercadorias jFTelaMercadorias;
    private static ListarMercadorias listarMercadorias;
    private static JFTelaCaixaAbrir jFTelaCaixaAbrir;
    private static JFTelaCaixaFechar jFTelaCaixaFechar;
    private static JFTelaVendas jFTelaVendas;
    private static JFTelaRetiradas jFTelaRetiradas;
    private static JFTelaRelatorios jFTelaRelatorios;
    private static JFRelatorioEstoque jFRelatorioEstoque;
    private static JFRelatorioVendas jFRelatorioVendas;
    private static JFRelatorioFinanceiro jFRelatorioFinanceiro;
    
    public static JFTelaLogin getLogin(){
        if(jFTelaLogin==null){
            jFTelaLogin = new JFTelaLogin();
        }
        return jFTelaLogin;
    }
    
    public static JFTelaInicial getInicial(){
        if(jFTelaInicial==null){
            jFTelaInicial = new JFTelaInicial();
        }
        return jFTelaInicial;
    }
    
    public static JFTelaNotaFiscal getNotaFiscal(){
        if(jFTelaNotaFiscal==null){
            jFTelaNotaFiscal = new JFTelaNotaFiscal();
        }
        return jFTelaNotaFiscal;
    }
    
    public static ListarNotasFiscais getListarNotasFiscais(){
        if(listarNotasFiscais==null){
            listarNotasFiscais = new ListarNotasFiscais();
        }
        return listarNotasFiscais;
    }
    
    public static JFTelaUsuarios getUsuario(){
        if(jFTelaUsuarios==null){
            jFTelaUsuarios = new JFTelaUsuarios();
        }
        return jFTelaUsuarios;
    }
    
    public static ListarUsuarios getListarUsuarios(){
        if(listarUsuarios==null){
            listarUsuarios = new ListarUsuarios();
        }
        return listarUsuarios;
    }
    
    public static JFTelaFornecedores getFornecedor(){
        if(jFTelaFornecedores==null){
            jFTelaFornecedores = new JFTelaFornecedores();
        }
        return jFTelaFornecedores;
    }
    
    public static ListarFornecedores getListarFornecedores(){
        if(listarFornecedores==null){
            listarFornecedores = new ListarFornecedores();
        }
        return listarFornecedores;
    }
    
    public static JFTelaMercadorias getMercadoria(){
        if(jFTelaMercadorias==null){
            jFTelaMercadorias = new JFTelaMercadorias();
        }
        return jFTelaMercadorias;
    }
    
    public static ListarMercadorias getListarMercadorias(){
        if(listarMercadorias==null){
            listarMercadorias = new ListarMercadorias();
        }
        return listarMercadorias;
    }
    
    public static JFTelaCaixaAbrir getTelaCaixaAbrir(){
        if(jFTelaCaixaAbrir==null){
            jFTelaCaixaAbrir = new JFTelaCaixaAbrir();
        }
        return jFTelaCaixaAbrir;
    }
    
    public static JFTelaCaixaFechar getTelaCaixaFechar(){
        if(jFTelaCaixaFechar==null){
            jFTelaCaixaFechar = new JFTelaCaixaFechar();
        }
        return jFTelaCaixaFechar;
    }
    
    public static JFTelaVendas getVendas(){
        if(jFTelaVendas==null){
            jFTelaVendas = new JFTelaVendas();
        }
        return jFTelaVendas;
    }
    
    public static JFTelaRetiradas getRetiradas(){
        if(jFTelaRetiradas==null){
            jFTelaRetiradas = new JFTelaRetiradas();
        }
        return jFTelaRetiradas;
    }
    
    public static JFTelaRelatorios getRelatorios(){
        if(jFTelaRelatorios==null){
            jFTelaRelatorios = new JFTelaRelatorios();
        }
        return jFTelaRelatorios;
    }
    
    public static JFRelatorioEstoque getRelatorioEstoque(){
        if(jFRelatorioEstoque==null){
            jFRelatorioEstoque = new JFRelatorioEstoque();
        }
        return jFRelatorioEstoque;
    }
    
    public static JFRelatorioVendas getRelatorioVendas(){
        if(jFRelatorioVendas==null){
            jFRelatorioVendas = new JFRelatorioVendas();
        }
        return jFRelatorioVendas;
    }
    
    public static JFRelatorioFinanceiro getRelatorioFinanceiro(){
        if(jFRelatorioFinanceiro==null){
            jFRelatorioFinanceiro = new JFRelatorioFinanceiro();
        }
        return jFRelatorioFinanceiro;
    }
}