package DomainModel;
import javax.persistence.*;

@Entity
@Table(name = "notaMercadoria")

public class NotaMercadoria {
    @Id
    private int idNota;
    @Id
    private int idMercadoria;
    @Column
    private String descricao;
    @Column
    private int quantidade;
    @Column
    private float precoUnitario;
    
    public NotaMercadoria(){}

    public int getIdNota() {
        return idNota;
    }

    public void setIdNota(int idNota) {
        this.idNota = idNota;
    }

    public int getIdMercadoria() {
        return idMercadoria;
    }

    public void setIdMercadoria(int idMercadoria) {
        this.idMercadoria = idMercadoria;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public float getPrecoUnitario() {
        return precoUnitario;
    }

    public void setPrecoUnitario(float precoUnitario) {
        this.precoUnitario = precoUnitario;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
