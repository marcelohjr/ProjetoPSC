package DomainModel;
import javax.persistence.*;

@Entity
@Table(name = "vendaMercadoria")

public class VendaMercadoria {
    @Id
    private long idVenda;
    @Id
    private long idMercadoria;
    @Column
    private String descricao;
    @Column
    private int quantidade;
    @Column
    private float precoUnitario;

    public long getIdVenda() {
        return idVenda;
    }

    public void setIdVenda(long idVenda) {
        this.idVenda = idVenda;
    }

    public long getIdMercadoria() {
        return idMercadoria;
    }

    public void setIdMercadoria(long idMercadoria) {
        this.idMercadoria = idMercadoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public float getPrecoUnitario() {
        return precoUnitario;
    }

    public void setPrecoUnitario(float precoUnitario) {
        this.precoUnitario = precoUnitario;
    }
}
