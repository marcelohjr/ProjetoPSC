package DomainModel;

import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "caixa")

public class Caixa {
    
    @Id
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date data;
    @Column
    private float saldoInicial;
    @Column
    private float totalVendas;
    @Column
    private float saldoFinal;
    @Column
    private float retiradas;
    @Column
    private boolean fechado;
    
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public float getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(float saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public float getTotalVendas() {
        return totalVendas;
    }

    public void setTotalVendas(float totalVendas) {
        this.totalVendas = totalVendas;
    }

    public float getSaldoFinal() {
        return saldoFinal;
    }
    
    public void setSaldoFinal(float saldoFinal) {
        this.saldoFinal = saldoFinal;
    }

    public boolean isFechado() {
        return fechado;
    }

    public void setFechado(boolean fechado) {
        this.fechado = fechado;
    }
    
    public float getRetiradas() {
        return retiradas;
    }

    public void setRetiradas(float retiradas) {
        this.retiradas = retiradas;
    }
}
