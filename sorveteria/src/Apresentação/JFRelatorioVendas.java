package Apresentação;

import DataAccess.VendaDAO;
import DomainModel.Venda;
import static Validações.validaData.compareData;
import static Validações.validaData.isData;
import static Validações.validaData.isDataMenorHoje;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class JFRelatorioVendas extends javax.swing.JFrame {

    public JFRelatorioVendas() {
        initComponents();
        jDCDataInicio.setDate(new Date());
        jDCDataFim.setDate(new Date());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTVendas = new javax.swing.JTable();
        jBFiltrar = new javax.swing.JButton();
        jBListar = new javax.swing.JButton();
        jBFechar = new javax.swing.JButton();
        jDCDataInicio = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jDCDataFim = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Vendas");
        jLabel2.setToolTipText("");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTVendas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Data", "Preço de Venda"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTVendas.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(jTVendas);
        if (jTVendas.getColumnModel().getColumnCount() > 0) {
            jTVendas.getColumnModel().getColumn(0).setResizable(false);
            jTVendas.getColumnModel().getColumn(0).setPreferredWidth(50);
            jTVendas.getColumnModel().getColumn(1).setResizable(false);
            jTVendas.getColumnModel().getColumn(2).setResizable(false);
            jTVendas.getColumnModel().getColumn(2).setPreferredWidth(50);
        }

        jBFiltrar.setText("Filtrar");
        jBFiltrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBFiltrarActionPerformed(evt);
            }
        });

        jBListar.setText("Listar Todos");
        jBListar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBListarActionPerformed(evt);
            }
        });

        jBFechar.setText("Fechar");
        jBFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBFecharActionPerformed(evt);
            }
        });

        jLabel1.setText("Data Início");

        jLabel5.setText("Data Fim");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(301, 301, 301)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jBFechar))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jDCDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jDCDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(96, 96, 96)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jBFiltrar)
                        .addGap(140, 140, 140))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(94, 94, 94)
                        .addComponent(jBListar)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jDCDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jDCDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(25, 25, 25)
                        .addComponent(jBFiltrar)
                        .addGap(18, 18, 18)
                        .addComponent(jBListar)
                        .addGap(133, 133, 133)
                        .addComponent(jBFechar))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBFiltrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBFiltrarActionPerformed
        boolean valido = true;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dataInicio = formatter.format(jDCDataInicio.getDate());
        String dataFim = formatter.format(jDCDataFim.getDate());

        if (!isData(dataInicio) || !isDataMenorHoje(dataInicio)) {
            valido = false;
            JOptionPane.showMessageDialog(this, " Data de Início Inválida!");
        }

        if (!isData(dataFim) || !isDataMenorHoje(dataFim)) {
            valido = false;
            JOptionPane.showMessageDialog(this, " Data Final Inválida!");
        }

        if (valido) {
            try {
                if (compareData(dataInicio, dataFim) > 0) {
                    valido = false;
                    JOptionPane.showMessageDialog(this, " A Data de Início não pode ser maior que a Data Final!");
                }
            } catch (ParseException ex) {
                Logger.getLogger(JFTelaNotaFiscal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (valido) {
            readJTableForDate();
        }
    }//GEN-LAST:event_jBFiltrarActionPerformed

    private void jBListarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBListarActionPerformed
        readJTable();
    }//GEN-LAST:event_jBListarActionPerformed

    private void jBFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBFecharActionPerformed
        dispose();
    }//GEN-LAST:event_jBFecharActionPerformed

    public void readJTable() {
        ((JTextField) jDCDataInicio.getDateEditor()).setEditable(false);
        ((JTextField) jDCDataFim.getDateEditor()).setEditable(false);
        DefaultTableModel modelo = (DefaultTableModel) jTVendas.getModel();
        modelo.setNumRows(0);
        VendaDAO vdao = new VendaDAO();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String data;

        for (Venda v : vdao.findAll()) {

            data = formatter.format(v.getData());

            modelo.addRow(new Object[]{
                v.getCodigo(),
                data,
                v.getValorTotal()
            });
        }
    }

    public void readJTableForDate() {

        DefaultTableModel modelo = (DefaultTableModel) jTVendas.getModel();
        modelo.setNumRows(0);
        VendaDAO vdao = new VendaDAO();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dataInicio = formatter.format(jDCDataInicio.getDate());
        String dataFim = formatter.format(jDCDataFim.getDate());
        String data;

        for (Venda v : vdao.findAll()) {

            data = formatter.format(v.getData());

            try {
                if (compareData(data, dataInicio) >= 0 && compareData(data, dataFim) <= 0) {
                    modelo.addRow(new Object[]{
                        v.getCodigo(),
                        data,
                        v.getValorTotal()
                    });
                }
            } catch (ParseException ex) {
                Logger.getLogger(JFTelaRetiradas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFRelatorioVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFRelatorioVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFRelatorioVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFRelatorioVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFRelatorioVendas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBFechar;
    private javax.swing.JButton jBFiltrar;
    private javax.swing.JButton jBListar;
    private com.toedter.calendar.JDateChooser jDCDataFim;
    private com.toedter.calendar.JDateChooser jDCDataInicio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTVendas;
    // End of variables declaration//GEN-END:variables
}
