package Apresentação;

import javax.swing.table.DefaultTableModel;
import DataAccess.UsuarioDAO;
import DomainModel.FContexto;
import DomainModel.Usuario;
import javax.swing.JOptionPane;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author rubensjr
 */
public class ListarUsuarios extends javax.swing.JFrame {
    
    public ListarUsuarios() {
        initComponents();
        DefaultTableModel modelo = (DefaultTableModel) jTUsuarios.getModel();
        jTUsuarios.setRowSorter(new TableRowSorter(modelo));
        readJTable();
    }
    
    public void readJTable() {
        
        DefaultTableModel modelo = (DefaultTableModel) jTUsuarios.getModel();
        modelo.setNumRows(0);
        UsuarioDAO udao = new UsuarioDAO();
        String tipo;

        for (Usuario u : udao.findAll()) {
            
            if(u.getTipo()==1)
                tipo = "Administrador";
            else tipo = "Usuário";
            
            modelo.addRow(new Object[]{
                u.getCodigo(),
                u.getNome(),
                tipo
            });
        }
    }
    
    public void readJTableForDesc(){
        DefaultTableModel modelo = (DefaultTableModel) jTUsuarios.getModel();
        modelo.setNumRows(0);
        UsuarioDAO udao = new UsuarioDAO();
        String tipo;

        for (Usuario u : udao.findAll()) {
            if(u.getNome().toUpperCase().startsWith(jTFBuscaUsuarios.getText().toUpperCase())){
                if(u.getTipo()==1)
                    tipo = "Administrador";
                else tipo = "Usuário";

                modelo.addRow(new Object[]{
                    u.getCodigo(),
                    u.getNome(),
                    tipo
                });
            }    
        }
    }
    
    public void readJTableForId(){
        DefaultTableModel modelo = (DefaultTableModel) jTUsuarios.getModel();
        modelo.setNumRows(0);
        UsuarioDAO udao = new UsuarioDAO();
        String tipo;

        for (Usuario u : udao.findAll()) {
            if(u.getCodigo() == Integer.parseInt(jTFBuscaUsuarios.getText())){
                if(u.getTipo()==1)
                    tipo = "Administrador";
                else tipo = "Usuário";

                modelo.addRow(new Object[]{
                    u.getCodigo(),
                    u.getNome(),
                    tipo
                });
            }    
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTUsuarios = new javax.swing.JTable();
        jBSelecionar = new javax.swing.JButton();
        jBBuscar = new javax.swing.JButton();
        jTFBuscaUsuarios = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jCBTipoPesquisa = new javax.swing.JComboBox<>();
        Fechar = new javax.swing.JButton();
        jBListarTodos = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nome", "Tipo"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTUsuarios.getTableHeader().setReorderingAllowed(false);
        jTUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTUsuariosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTUsuarios);
        if (jTUsuarios.getColumnModel().getColumnCount() > 0) {
            jTUsuarios.getColumnModel().getColumn(0).setResizable(false);
            jTUsuarios.getColumnModel().getColumn(0).setPreferredWidth(100);
            jTUsuarios.getColumnModel().getColumn(1).setResizable(false);
            jTUsuarios.getColumnModel().getColumn(1).setPreferredWidth(350);
            jTUsuarios.getColumnModel().getColumn(2).setResizable(false);
        }

        jBSelecionar.setText("Selecionar");
        jBSelecionar.setEnabled(false);
        jBSelecionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSelecionarActionPerformed(evt);
            }
        });

        jBBuscar.setText("Buscar");
        jBBuscar.setEnabled(false);
        jBBuscar.setMaximumSize(new java.awt.Dimension(96, 18));
        jBBuscar.setMinimumSize(new java.awt.Dimension(96, 18));
        jBBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarActionPerformed(evt);
            }
        });

        jTFBuscaUsuarios.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFBuscaUsuariosKeyReleased(evt);
            }
        });

        jLabel1.setText("Pesquisar por:");

        jCBTipoPesquisa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Código", "Nome" }));
        jCBTipoPesquisa.setMaximumSize(new java.awt.Dimension(70, 18));
        jCBTipoPesquisa.setMinimumSize(new java.awt.Dimension(70, 18));

        Fechar.setText("Fechar");
        Fechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FecharActionPerformed(evt);
            }
        });

        jBListarTodos.setText("Listar Todos");
        jBListarTodos.setEnabled(false);
        jBListarTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBListarTodosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCBTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                        .addComponent(jTFBuscaUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(jBBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jBListarTodos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jBSelecionar)
                        .addGap(18, 18, 18)
                        .addComponent(Fechar, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCBTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jBBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTFBuscaUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 297, Short.MAX_VALUE)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBSelecionar)
                    .addComponent(Fechar)
                    .addComponent(jBListarTodos))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarActionPerformed
        if(jCBTipoPesquisa.getSelectedItem().equals("Nome"))
            readJTableForDesc();
        else{
            try{
                readJTableForId();
            }catch(NumberFormatException ex){
                readJTable();
                JOptionPane.showMessageDialog(this, " Código Inválido!");
                jTFBuscaUsuarios.setText("");
            }
        }
    }//GEN-LAST:event_jBBuscarActionPerformed

    
    private void FecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FecharActionPerformed
        dispose();
    }//GEN-LAST:event_FecharActionPerformed

    private void jBSelecionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSelecionarActionPerformed
        if (jTUsuarios.getSelectedRow() != -1) {
            Usuario u = new Usuario();
            u = UsuarioDAO.getInstance().getById((int)jTUsuarios.getValueAt(jTUsuarios.getSelectedRow(), 0));
        
            FContexto.getUsuario().setUsuario(u);
            dispose();
        }
    }//GEN-LAST:event_jBSelecionarActionPerformed

    private void jTUsuariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTUsuariosMouseClicked
        if (jTUsuarios.getSelectedRow() != -1) {
            jBSelecionar.setEnabled(true);
        }
    }//GEN-LAST:event_jTUsuariosMouseClicked

    private void jTFBuscaUsuariosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFBuscaUsuariosKeyReleased
        if(jTFBuscaUsuarios.getText().equals(""))
            jBBuscar.setEnabled(false);
        else 
            jBBuscar.setEnabled(true);
        
        jBListarTodos.setEnabled(true);
    }//GEN-LAST:event_jTFBuscaUsuariosKeyReleased

    private void jBListarTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBListarTodosActionPerformed
        readJTable();
        jBListarTodos.setEnabled(false);
    }//GEN-LAST:event_jBListarTodosActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ListarUsuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ListarUsuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ListarUsuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ListarUsuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fjTUsuarios       /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ListarUsuarios().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Fechar;
    private javax.swing.JButton jBBuscar;
    private javax.swing.JButton jBListarTodos;
    private javax.swing.JButton jBSelecionar;
    private javax.swing.JComboBox<String> jCBTipoPesquisa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTFBuscaUsuarios;
    private javax.swing.JTable jTUsuarios;
    // End of variables declaration//GEN-END:variables
}
