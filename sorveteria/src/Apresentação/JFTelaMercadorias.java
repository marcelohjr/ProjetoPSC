package Apresentação;

import DataAccess.FornecedorDAO;
import DataAccess.MercadoriaDAO;
import DataAccess.MercadoriaFornecedorDAO;
import DomainModel.FContexto;
import DomainModel.Fornecedor;
import DomainModel.Mercadoria;
import DomainModel.MercadoriaFornecedor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class JFTelaMercadorias extends javax.swing.JFrame {

    public JFTelaMercadorias() {
        initComponents();
        DefaultTableModel modelo = (DefaultTableModel) jTFornecedores.getModel();
        jTFornecedores.setRowSorter(new TableRowSorter(modelo));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFrame1 = new javax.swing.JFrame();
        jLabel1 = new javax.swing.JLabel();
        jTFCodigo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTFData = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        jTFNome = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jCBUnidade = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jTFEstoqueMinimo = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTFEstoqueAtual = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTFornecedores = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        jBNovo = new javax.swing.JButton();
        jBBuscar = new javax.swing.JButton();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBSalvar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();
        jTFPrecoCompra = new javax.swing.JFormattedTextField();
        jTFPrecoVenda = new javax.swing.JFormattedTextField();
        jSeparator1 = new javax.swing.JSeparator();

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tela de Produtos");

        jLabel1.setText("Código");

        jTFCodigo.setEnabled(false);

        jLabel2.setText("Data de Cadastro");

        jTFData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));
        jTFData.setEnabled(false);

        jLabel3.setText("Descrição");

        jTFNome.setEnabled(false);
        jTFNome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFNomeKeyReleased(evt);
            }
        });

        jLabel4.setText("Unidade");

        jCBUnidade.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "UN", "CX", "PCT", "BD", "DZ" }));
        jCBUnidade.setEnabled(false);

        jLabel5.setText("Estoque Mínimo");

        jTFEstoqueMinimo.setEnabled(false);
        jTFEstoqueMinimo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFEstoqueMinimoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTFEstoqueMinimoKeyTyped(evt);
            }
        });

        jLabel6.setText("Estoque Atual");

        jTFEstoqueAtual.setEnabled(false);
        jTFEstoqueAtual.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFEstoqueAtualKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTFEstoqueAtualKeyTyped(evt);
            }
        });

        jLabel7.setText("Preço de Compra");

        jLabel8.setText("Preço de Venda");

        jTFornecedores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nome", "CPF / CNPJ"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTFornecedores.setEnabled(false);
        jScrollPane1.setViewportView(jTFornecedores);
        if (jTFornecedores.getColumnModel().getColumnCount() > 0) {
            jTFornecedores.getColumnModel().getColumn(0).setResizable(false);
            jTFornecedores.getColumnModel().getColumn(0).setPreferredWidth(70);
            jTFornecedores.getColumnModel().getColumn(1).setResizable(false);
            jTFornecedores.getColumnModel().getColumn(1).setPreferredWidth(350);
            jTFornecedores.getColumnModel().getColumn(2).setResizable(false);
            jTFornecedores.getColumnModel().getColumn(2).setPreferredWidth(130);
        }

        jLabel9.setText("Fornecedores");

        jBNovo.setText("Novo");
        jBNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBNovoActionPerformed(evt);
            }
        });

        jBBuscar.setText("Buscar");
        jBBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarActionPerformed(evt);
            }
        });

        jBAlterar.setText("Alterar");
        jBAlterar.setEnabled(false);
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });

        jBExcluir.setText("Excluir");
        jBExcluir.setEnabled(false);
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });

        jBSalvar.setText("Salvar");
        jBSalvar.setEnabled(false);
        jBSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSalvarActionPerformed(evt);
            }
        });

        jBCancelar.setText("Cancelar");
        jBCancelar.setEnabled(false);
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });

        jTFPrecoCompra.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));
        jTFPrecoCompra.setEnabled(false);
        jTFPrecoCompra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFPrecoCompraKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTFPrecoCompraKeyTyped(evt);
            }
        });

        jTFPrecoVenda.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));
        jTFPrecoVenda.setEnabled(false);
        jTFPrecoVenda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFPrecoVendaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTFPrecoVendaKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(285, 285, 285)
                                .addComponent(jLabel9))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(41, 41, 41)
                                .addComponent(jBNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jBBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jBAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jBExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jBSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jBCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 26, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel1)
                                            .addComponent(jLabel3))
                                        .addGap(23, 23, 23)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(45, 45, 45)
                                                .addComponent(jLabel2)
                                                .addGap(18, 18, 18)
                                                .addComponent(jTFData, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(jTFNome))
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel4)
                                        .addGap(18, 18, 18)
                                        .addComponent(jCBUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel5)
                                            .addComponent(jLabel6))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jTFEstoqueMinimo)
                                            .addComponent(jTFEstoqueAtual, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jTFPrecoCompra, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                            .addComponent(jTFPrecoVenda))))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jSeparator1))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jTFData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTFNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jCBUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTFEstoqueMinimo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jTFPrecoCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTFEstoqueAtual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(jTFPrecoVenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBNovo)
                    .addComponent(jBBuscar)
                    .addComponent(jBAlterar)
                    .addComponent(jBExcluir)
                    .addComponent(jBSalvar)
                    .addComponent(jBCancelar))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jTFEstoqueMinimoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFEstoqueMinimoKeyTyped
        String caracteres = "0987654321";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_jTFEstoqueMinimoKeyTyped

    private void jTFEstoqueAtualKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFEstoqueAtualKeyTyped
        String caracteres = "0987654321";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_jTFEstoqueAtualKeyTyped

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        inicio();
    }//GEN-LAST:event_jBCancelarActionPerformed

    private void jTFNomeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFNomeKeyReleased
        if (verificaCampos()) {
            jBSalvar.setEnabled(true);
        } else {
            jBSalvar.setEnabled(false);
        }
    }//GEN-LAST:event_jTFNomeKeyReleased

    private void jTFEstoqueMinimoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFEstoqueMinimoKeyReleased
        if (verificaCampos()) {
            jBSalvar.setEnabled(true);
        } else {
            jBSalvar.setEnabled(false);
        }
    }//GEN-LAST:event_jTFEstoqueMinimoKeyReleased

    private void jTFEstoqueAtualKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFEstoqueAtualKeyReleased
        if (verificaCampos()) {
            jBSalvar.setEnabled(true);
        } else {
            jBSalvar.setEnabled(false);
        }
    }//GEN-LAST:event_jTFEstoqueAtualKeyReleased

    private void jTFPrecoCompraKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFPrecoCompraKeyReleased
        if (verificaCampos()) {
            jBSalvar.setEnabled(true);
        } else {
            jBSalvar.setEnabled(false);
        }
    }//GEN-LAST:event_jTFPrecoCompraKeyReleased

    private void jTFPrecoVendaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFPrecoVendaKeyReleased
        if (verificaCampos()) {
            jBSalvar.setEnabled(true);
        } else {
            jBSalvar.setEnabled(false);
        }
    }//GEN-LAST:event_jTFPrecoVendaKeyReleased

    private void jBNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBNovoActionPerformed
        String data;
        limpar();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        data = formatter.format(new Date());
        jTFData.setText(data);
        habilitarCampos();
        jBCancelar.setEnabled(true);
        jBNovo.setEnabled(false);
    }//GEN-LAST:event_jBNovoActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        MercadoriaFornecedorDAO mfdao = new MercadoriaFornecedorDAO();
        int codigo = Integer.parseInt(jTFCodigo.getText());
        MercadoriaDAO.getInstance().removeById(codigo);

        for (MercadoriaFornecedor mf : mfdao.findAll()) {
            if (mf.getIdMercadoria() == codigo) {
                mfdao.removeById(codigo);
            }
        }

        JOptionPane.showMessageDialog(this, " Produto excluído com sucesso!");
        inicio();
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
        habilitarCampos();
        jBAlterar.setEnabled(false);
        jBBuscar.setEnabled(false);
        jBSalvar.setEnabled(true);
        jBNovo.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBCancelar.setEnabled(true);
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSalvarActionPerformed
        Mercadoria mercadoria = new Mercadoria();
        MercadoriaDAO mdao = new MercadoriaDAO();
        boolean valido = true;

        String precoCompra = jTFPrecoCompra.getText();
        String precoVenda = jTFPrecoVenda.getText();
        precoCompra = precoCompra.replace(".", "");
        precoVenda = precoVenda.replace(".", "");
        precoCompra = precoCompra.replace(",", ".");
        precoVenda = precoVenda.replace(",", ".");

        for (Mercadoria m : mdao.findAll()) {
            if (m.getNome().equals(jTFNome.getText())) {
                if (jTFCodigo.getText().equals("")) {
                    valido = false;
                    break;
                }
            }
        }

        if (!valido) {
            JOptionPane.showMessageDialog(this, " Já existe um produto com essa descrição!");
        }

        try{
            if (Float.parseFloat(precoCompra) > Float.parseFloat(precoVenda)) {
                valido = false;
                JOptionPane.showMessageDialog(this, " Preço de Venda Inválido!");
            }
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, " Preços Inválidos!");
           valido = false;
        }    
            

        if (valido) {

            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            Date data = null;

            try {
                data = formato.parse(jTFData.getText());
            } catch (ParseException ex) {
                Logger.getLogger(JFTelaMercadorias.class.getName()).log(Level.SEVERE, null, ex);
            }

            mercadoria.setData(data);
            mercadoria.setNome(jTFNome.getText());
            mercadoria.setUnidade((String) jCBUnidade.getSelectedItem());
            mercadoria.setEstoqueMinimo(Integer.parseInt(jTFEstoqueMinimo.getText()));
            mercadoria.setEstoqueAtual(Integer.parseInt(jTFEstoqueAtual.getText()));
            mercadoria.setPrecoCompra(Float.parseFloat(precoCompra));
            mercadoria.setPrecoVenda(Float.parseFloat(precoVenda));

            if (jTFCodigo.getText().equals("")) {
                MercadoriaDAO.getInstance().persist(mercadoria);
            } else {
                mercadoria.setCodigo(Integer.parseInt(jTFCodigo.getText()));
                MercadoriaDAO.getInstance().merge(mercadoria);
            }

            JOptionPane.showMessageDialog(this, " Salvo com sucesso!");
            inicio();
        }
    }//GEN-LAST:event_jBSalvarActionPerformed

    private void jTFPrecoCompraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFPrecoCompraKeyTyped
        String caracteres = "0987654321,";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_jTFPrecoCompraKeyTyped

    private void jTFPrecoVendaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFPrecoVendaKeyTyped
        String caracteres = "0987654321,";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_jTFPrecoVendaKeyTyped

    private void jBBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarActionPerformed
        FContexto.getListarMercadorias().setVisible(true);
        FContexto.getListarMercadorias().readJTable();
    }//GEN-LAST:event_jBBuscarActionPerformed

    public void setMercadoria(Mercadoria mercadoria) {
        DefaultTableModel modelo = (DefaultTableModel) jTFornecedores.getModel();
        modelo.setNumRows(0);
        MercadoriaFornecedorDAO mfdao = new MercadoriaFornecedorDAO();
        Fornecedor fornecedor;
        String id;

        String precoCompra = String.valueOf(mercadoria.getPrecoCompra());
        String precoVenda = String.valueOf(mercadoria.getPrecoVenda());

        precoCompra = precoCompra.replace(".", ",");
        precoVenda = precoVenda.replace(".", ",");

        jTFCodigo.setText(String.valueOf(mercadoria.getCodigo()));

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        jTFData.setText(formato.format(mercadoria.getData()));

        jTFNome.setText(mercadoria.getNome());
        jCBUnidade.setSelectedItem(mercadoria.getUnidade());
        jTFEstoqueMinimo.setText(String.valueOf(mercadoria.getEstoqueMinimo()));
        jTFEstoqueAtual.setText(String.valueOf(mercadoria.getEstoqueAtual()));
        jTFPrecoCompra.setText(precoCompra);
        jTFPrecoVenda.setText(precoVenda);

        for (MercadoriaFornecedor mf : mfdao.findAll()) {
            if (mf.getIdMercadoria() == mercadoria.getCodigo()) {
                fornecedor = new Fornecedor();
                fornecedor = FornecedorDAO.getInstance().getById(mf.getIdFornecedor());

                if (fornecedor.getCnpj().equals("")) {
                    id = fornecedor.getCpf();
                } else {
                    id = fornecedor.getCnpj();
                }

                modelo.addRow(new Object[]{
                    fornecedor.getCodigo(),
                    fornecedor.getNome(),
                    id
                });
            }
        }

        jBExcluir.setEnabled(true);
        jBAlterar.setEnabled(true);
        jBCancelar.setEnabled(true);
        jBSalvar.setEnabled(false);
    }

    private void inicio() {
        limpar();
        desabilitarCampos();
        jBNovo.setEnabled(true);
        jBBuscar.setEnabled(true);
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBSalvar.setEnabled(false);
        jBCancelar.setEnabled(false);
    }

    void desabilitarCampos() {
        jTFNome.setEnabled(false);
        jCBUnidade.setEnabled(false);
        jTFEstoqueMinimo.setEnabled(false);
        jTFEstoqueAtual.setEnabled(false);
        jTFPrecoCompra.setEnabled(false);
        jTFPrecoVenda.setEnabled(false);
        jTFornecedores.setEnabled(false);
    }

    private void habilitarCampos() {
        jTFNome.setEnabled(true);
        jCBUnidade.setEnabled(true);
        jTFEstoqueMinimo.setEnabled(true);
        jTFEstoqueAtual.setEnabled(true);
        jTFPrecoCompra.setEnabled(true);
        jTFPrecoVenda.setEnabled(true);
        jTFornecedores.setEnabled(true);
    }

    private void limpar() {
        jTFCodigo.setText("");
        jTFData.setText("");
        jTFNome.setText("");
        jCBUnidade.setSelectedItem("UN");
        jTFEstoqueMinimo.setText("");
        jTFEstoqueAtual.setText("");
        jTFPrecoCompra.setText("");
        jTFPrecoVenda.setText("");
        DefaultTableModel modelo = (DefaultTableModel) jTFornecedores.getModel();
        modelo.setNumRows(0);
    }

    public boolean verificaCampos() {
        boolean resp = true;

        if (jTFNome.getText().equals("")) {
            resp = false;
        }
        if (jTFEstoqueMinimo.getText().equals("")) {
            resp = false;
        }
        if (jTFEstoqueAtual.getText().equals("")) {
            resp = false;
        }
        if (jTFPrecoCompra.getText().equals("")) {
            resp = false;
        }
        if (jTFPrecoVenda.getText().equals("")) {
            resp = false;
        }

        return resp;
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFTelaMercadorias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFTelaMercadorias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFTelaMercadorias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFTelaMercadorias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFTelaMercadorias().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBAlterar;
    private javax.swing.JButton jBBuscar;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBNovo;
    private javax.swing.JButton jBSalvar;
    private javax.swing.JComboBox<String> jCBUnidade;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jTFCodigo;
    private javax.swing.JFormattedTextField jTFData;
    private javax.swing.JTextField jTFEstoqueAtual;
    private javax.swing.JTextField jTFEstoqueMinimo;
    private javax.swing.JTextField jTFNome;
    private javax.swing.JFormattedTextField jTFPrecoCompra;
    private javax.swing.JFormattedTextField jTFPrecoVenda;
    private javax.swing.JTable jTFornecedores;
    // End of variables declaration//GEN-END:variables
}
