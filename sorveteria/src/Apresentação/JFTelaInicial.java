package Apresentação;

import DataAccess.CaixaDAO;
import DataAccess.RetiradasDAO;
import DomainModel.Caixa;
import DomainModel.FContexto;
import DomainModel.Retiradas;
import java.awt.Window;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class JFTelaInicial extends javax.swing.JFrame {

    public JFTelaInicial() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        JBVenda = new javax.swing.JButton();
        JBRelatorios = new javax.swing.JButton();
        JBAbrirCaixa = new javax.swing.JButton();
        JBFecharCaixa = new javax.swing.JButton();
        JMBMenu = new javax.swing.JMenuBar();
        JMUsuario = new javax.swing.JMenu();
        jMIUsuarios = new javax.swing.JMenuItem();
        JMProdutos = new javax.swing.JMenu();
        jMIProdutos = new javax.swing.JMenuItem();
        JMFornecedor = new javax.swing.JMenu();
        jMIFornecedor = new javax.swing.JMenuItem();
        JMRetiradas = new javax.swing.JMenu();
        JMAquisicaoProdutos = new javax.swing.JMenu();
        JMNotaFiscal = new javax.swing.JMenuItem();
        jSair = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tela Inicial");
        setBackground(new java.awt.Color(51, 51, 51));

        JBVenda.setBackground(new java.awt.Color(255, 255, 255));
        JBVenda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/Iniciar Venda.png"))); // NOI18N
        JBVenda.setText("Iniciar Venda");
        JBVenda.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        JBVenda.setVerifyInputWhenFocusTarget(false);
        JBVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBVendaActionPerformed(evt);
            }
        });

        JBRelatorios.setBackground(new java.awt.Color(255, 255, 255));
        JBRelatorios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/Relatório.png"))); // NOI18N
        JBRelatorios.setText("Relatórios");
        JBRelatorios.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        JBRelatorios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBRelatoriosActionPerformed(evt);
            }
        });

        JBAbrirCaixa.setBackground(new java.awt.Color(255, 255, 255));
        JBAbrirCaixa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/Abrir Caixa.png"))); // NOI18N
        JBAbrirCaixa.setText("Abrir Caixa");
        JBAbrirCaixa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        JBAbrirCaixa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBAbrirCaixaActionPerformed(evt);
            }
        });

        JBFecharCaixa.setBackground(new java.awt.Color(255, 255, 255));
        JBFecharCaixa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/Fechar Caixa.png"))); // NOI18N
        JBFecharCaixa.setText("Fechar Caixa");
        JBFecharCaixa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        JBFecharCaixa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBFecharCaixaActionPerformed(evt);
            }
        });

        JMBMenu.setBackground(new java.awt.Color(255, 255, 255));
        JMBMenu.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        JMBMenu.setPreferredSize(new java.awt.Dimension(240, 40));

        JMUsuario.setBackground(new java.awt.Color(0, 0, 0));
        JMUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/cliente.png"))); // NOI18N
        JMUsuario.setText("Usuários");
        JMUsuario.setPreferredSize(new java.awt.Dimension(100, 32));

        jMIUsuarios.setText("Gerenciar Usuários");
        jMIUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIUsuariosActionPerformed(evt);
            }
        });
        JMUsuario.add(jMIUsuarios);

        JMBMenu.add(JMUsuario);

        JMProdutos.setBackground(new java.awt.Color(255, 255, 255));
        JMProdutos.setBorder(null);
        JMProdutos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/produto.png"))); // NOI18N
        JMProdutos.setText("Produtos");
        JMProdutos.setPreferredSize(new java.awt.Dimension(100, 25));

        jMIProdutos.setText("Gerenciar Produtos");
        jMIProdutos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIProdutosActionPerformed(evt);
            }
        });
        JMProdutos.add(jMIProdutos);

        JMBMenu.add(JMProdutos);

        JMFornecedor.setBorder(null);
        JMFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/fornecedor.png"))); // NOI18N
        JMFornecedor.setText("Fornecedores");
        JMFornecedor.setPreferredSize(new java.awt.Dimension(120, 32));

        jMIFornecedor.setText("Gerenciar Fornecedores");
        jMIFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIFornecedorActionPerformed(evt);
            }
        });
        JMFornecedor.add(jMIFornecedor);

        JMBMenu.add(JMFornecedor);

        JMRetiradas.setBackground(new java.awt.Color(255, 255, 255));
        JMRetiradas.setBorder(null);
        JMRetiradas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/Retiradas.png"))); // NOI18N
        JMRetiradas.setText("Retiradas");
        JMRetiradas.setMaximumSize(new java.awt.Dimension(95, 32767));
        JMRetiradas.setPreferredSize(new java.awt.Dimension(90, 19));
        JMRetiradas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JMRetiradasMouseClicked(evt);
            }
        });
        JMBMenu.add(JMRetiradas);

        JMAquisicaoProdutos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/compra-foco.png"))); // NOI18N
        JMAquisicaoProdutos.setText("Aquisição de Produtos");
        JMAquisicaoProdutos.setPreferredSize(new java.awt.Dimension(170, 32));

        JMNotaFiscal.setText("Notas Fiscais");
        JMNotaFiscal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        JMNotaFiscal.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        JMNotaFiscal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JMNotaFiscalActionPerformed(evt);
            }
        });
        JMAquisicaoProdutos.add(JMNotaFiscal);

        JMBMenu.add(JMAquisicaoProdutos);

        jSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/sair.png"))); // NOI18N
        jSair.setText("Sair");
        jSair.setPreferredSize(new java.awt.Dimension(78, 32));
        jSair.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jSairMouseClicked(evt);
            }
        });
        JMBMenu.add(jSair);

        setJMenuBar(JMBMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(141, 141, 141)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(JBVenda, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                    .addComponent(JBAbrirCaixa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(86, 86, 86)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(JBRelatorios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JBFecharCaixa, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE))
                .addContainerGap(154, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(91, 91, 91)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JBVenda, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JBRelatorios, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JBAbrirCaixa, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JBFecharCaixa, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(69, 69, 69))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JMNotaFiscalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JMNotaFiscalActionPerformed
        FContexto.getNotaFiscal().setVisible(true);
    }//GEN-LAST:event_JMNotaFiscalActionPerformed

    private void jSairMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jSairMouseClicked
        Caixa caixa;
        CaixaDAO cdao = new CaixaDAO();
        RetiradasDAO rdao = new RetiradasDAO();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        String data = formato.format(new Date());
        boolean valido = false;
        float valorRetirada;
        Date dataHoje = null;

        try {
            dataHoje = formato.parse(data);
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaMercadorias.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Caixa cx : cdao.findAll()) {
            if (dataHoje.compareTo(cx.getData()) == 1) {
                valido = true;
                break;
            }
        }

        if (valido) {
            for (Caixa cx : cdao.findAll()) {
                if (dataHoje.compareTo(cx.getData()) == 1) {
                    caixa = cx;
                    valorRetirada = (float) 0.0;

                    for (Retiradas r : rdao.findAll()) {
                        if (cx.getData().compareTo(r.getData()) == 0) {
                            valorRetirada += r.getValor();
                        }
                    }

                    caixa.setRetiradas(valorRetirada);
                    caixa.setSaldoFinal((caixa.getSaldoInicial() + caixa.getTotalVendas()) - valorRetirada);
                    caixa.setFechado(true);
                    CaixaDAO.getInstance().merge(caixa);
                }
            }
        }

        System.gc();
        for (Window window : Window.getWindows()) {
            window.dispose();
        }

        FContexto.getLogin().setVisible(true);
        FContexto.getLogin().limpar();
    }//GEN-LAST:event_jSairMouseClicked

    private void JBAbrirCaixaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBAbrirCaixaActionPerformed
        boolean valido = false;
        CaixaDAO cdao = new CaixaDAO();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        String data = formato.format(new Date());
        Date dataHoje = null;

        try {
            dataHoje = formato.parse(data);
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaMercadorias.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Caixa cx : cdao.findAll()) {
            if (dataHoje.compareTo(cx.getData()) == 0) {
                valido = true;
                break;
            }
        }

        if (valido) {
            JOptionPane.showMessageDialog(this, " O caixa do dia já foi aberto!");
        } else {
            FContexto.getTelaCaixaAbrir().setVisible(true);
        }
    }//GEN-LAST:event_JBAbrirCaixaActionPerformed

    private void JBFecharCaixaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBFecharCaixaActionPerformed
        boolean valido = false;
        CaixaDAO cdao = new CaixaDAO();
        Caixa caixa = new Caixa();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        String data = formato.format(new Date());
        Date dataHoje = null;

        try {
            dataHoje = formato.parse(data);
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaMercadorias.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Caixa cx : cdao.findAll()) {
            if (dataHoje.compareTo(cx.getData()) == 0) {
                valido = true;
                caixa = cx;
                break;
            }
        }

        if (valido && !caixa.isFechado()) {
            FContexto.getTelaCaixaFechar().setVisible(true);
            FContexto.getTelaCaixaFechar().atualizaCaixa();
        } else if (valido && caixa.isFechado()) {
            JOptionPane.showMessageDialog(this, " O caixa já foi fechado!");
        } else {
            JOptionPane.showMessageDialog(this, " O caixa do dia ainda não foi aberto!");
        }
    }//GEN-LAST:event_JBFecharCaixaActionPerformed

    private void JBVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBVendaActionPerformed
        boolean valido = false;
        CaixaDAO cdao = new CaixaDAO();
        Caixa caixa = new Caixa();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        String data = formato.format(new Date());
        Date dataHoje = null;

        try {
            dataHoje = formato.parse(data);
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaMercadorias.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Caixa cx : cdao.findAll()) {
            if (dataHoje.compareTo(cx.getData()) == 0) {
                valido = true;
                caixa = cx;
                break;
            }
        }

        if (valido && !caixa.isFechado()) {
            FContexto.getVendas().setVisible(true);
        } else if (valido && caixa.isFechado()) {
            JOptionPane.showMessageDialog(this, " O caixa já foi fechado!");
        } else {
            JOptionPane.showMessageDialog(this, " O caixa do dia ainda não foi aberto!");
        }
    }//GEN-LAST:event_JBVendaActionPerformed

    private void JMRetiradasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JMRetiradasMouseClicked
        boolean valido = false;
        CaixaDAO cdao = new CaixaDAO();
        Caixa caixa = new Caixa();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        String data = formato.format(new Date());
        Date dataHoje = null;

        try {
            dataHoje = formato.parse(data);
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaMercadorias.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Caixa cx : cdao.findAll()) {
            if (dataHoje.compareTo(cx.getData()) == 0) {
                valido = true;
                caixa = cx;
                break;
            }
        }

        if (!valido) {
            JOptionPane.showMessageDialog(this, " O caixa do dia ainda não foi aberto!");
            FContexto.getRetiradas().setVisible(true);
            FContexto.getRetiradas().caixaInvalido();
        }

        if (valido && caixa.isFechado()) {
            JOptionPane.showMessageDialog(this, " O caixa do dia já foi fechado!");
            FContexto.getRetiradas().setVisible(true);
            FContexto.getRetiradas().caixaInvalido();
        }

        FContexto.getRetiradas().setVisible(true);
    }//GEN-LAST:event_JMRetiradasMouseClicked

    private void jMIUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIUsuariosActionPerformed
        FContexto.getUsuario().setVisible(true);
    }//GEN-LAST:event_jMIUsuariosActionPerformed

    private void jMIProdutosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIProdutosActionPerformed
        FContexto.getMercadoria().setVisible(true);
    }//GEN-LAST:event_jMIProdutosActionPerformed

    private void jMIFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIFornecedorActionPerformed
        FContexto.getFornecedor().setVisible(true);
    }//GEN-LAST:event_jMIFornecedorActionPerformed

    private void JBRelatoriosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBRelatoriosActionPerformed
        FContexto.getRelatorios().setVisible(true);
    }//GEN-LAST:event_JBRelatoriosActionPerformed

    void verificaTipoUsuario(int tipo) {
        if (tipo == 2) {
            JMUsuario.setEnabled(false);
            jMIUsuarios.setEnabled(false);
            JMFornecedor.setEnabled(false);
            jMIFornecedor.setEnabled(false);
            JMProdutos.setEnabled(false);
            jMIProdutos.setEnabled(false);
            JMNotaFiscal.setEnabled(false);
            JMAquisicaoProdutos.setEnabled(false);
            JBRelatorios.setEnabled(false);
            JOptionPane.showMessageDialog(this, " Usuário: Comum");
        }else{
            JMUsuario.setEnabled(true);
            jMIUsuarios.setEnabled(true);
            JMFornecedor.setEnabled(true);
            jMIFornecedor.setEnabled(true);
            JMProdutos.setEnabled(true);
            jMIProdutos.setEnabled(true);
            JMNotaFiscal.setEnabled(true);
            JMAquisicaoProdutos.setEnabled(true);
            JBRelatorios.setEnabled(true);
            JOptionPane.showMessageDialog(this, " Usuário: Administrador");
        }
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFTelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFTelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFTelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFTelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFTelaInicial().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBAbrirCaixa;
    private javax.swing.JButton JBFecharCaixa;
    private javax.swing.JButton JBRelatorios;
    private javax.swing.JButton JBVenda;
    private javax.swing.JMenu JMAquisicaoProdutos;
    private javax.swing.JMenuBar JMBMenu;
    private javax.swing.JMenu JMFornecedor;
    private javax.swing.JMenuItem JMNotaFiscal;
    private javax.swing.JMenu JMProdutos;
    private javax.swing.JMenu JMRetiradas;
    private javax.swing.JMenu JMUsuario;
    private javax.swing.JMenuItem jMIFornecedor;
    private javax.swing.JMenuItem jMIProdutos;
    private javax.swing.JMenuItem jMIUsuarios;
    private javax.swing.JMenu jSair;
    // End of variables declaration//GEN-END:variables
}
