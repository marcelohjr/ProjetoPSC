package Apresentação;

import DataAccess.UsuarioDAO;
import DomainModel.Usuario;
import javax.swing.JOptionPane;
import static Criptografia.Criptografia.criptografar;
import DomainModel.FContexto;

//@author marcelo
  
public class JFTelaUsuarios extends javax.swing.JFrame {
    
    public JFTelaUsuarios() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTFCodigo = new javax.swing.JTextField();
        jTFNome = new javax.swing.JTextField();
        jTFLogin = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jBBuscar = new javax.swing.JButton();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBSalvar = new javax.swing.JButton();
        jTFSenha = new javax.swing.JPasswordField();
        jBCTipo = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jBNovo = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tela de Usuários");
        setBackground(java.awt.Color.white);
        setMaximumSize(new java.awt.Dimension(662, 396));
        setMinimumSize(new java.awt.Dimension(662, 396));
        setPreferredSize(new java.awt.Dimension(662, 396));
        setSize(new java.awt.Dimension(662, 396));

        jTFCodigo.setEditable(false);
        jTFCodigo.setEnabled(false);
        jTFCodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFCodigoActionPerformed(evt);
            }
        });

        jTFNome.setEnabled(false);
        jTFNome.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                jTFNomeInputMethodTextChanged(evt);
            }
        });
        jTFNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFNomeActionPerformed(evt);
            }
        });
        jTFNome.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTFNomePropertyChange(evt);
            }
        });
        jTFNome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFNomeKeyReleased(evt);
            }
        });

        jTFLogin.setEnabled(false);
        jTFLogin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFLoginKeyReleased(evt);
            }
        });

        jLabel1.setText("Código");

        jLabel2.setText("Nome");

        jLabel3.setText("Login");

        jLabel4.setText("Senha");

        jBBuscar.setText("Buscar");
        jBBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarActionPerformed(evt);
            }
        });

        jBAlterar.setText("Alterar");
        jBAlterar.setEnabled(false);
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });

        jBExcluir.setText("Excluir");
        jBExcluir.setEnabled(false);
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });

        jBSalvar.setText("Salvar");
        jBSalvar.setEnabled(false);
        jBSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSalvarActionPerformed(evt);
            }
        });

        jTFSenha.setEnabled(false);
        jTFSenha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFSenhaActionPerformed(evt);
            }
        });
        jTFSenha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFSenhaKeyReleased(evt);
            }
        });

        jBCTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Administrador", "Usuário" }));
        jBCTipo.setEnabled(false);
        jBCTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCTipoActionPerformed(evt);
            }
        });

        jLabel6.setText("Tipo de Usuário");

        jBNovo.setText("Novo");
        jBNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBNovoActionPerformed(evt);
            }
        });

        jBCancelar.setText("Cancelar");
        jBCancelar.setEnabled(false);
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jBNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTFNome, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
                            .addComponent(jTFLogin, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
                            .addComponent(jTFSenha)
                            .addComponent(jTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jBCTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jBBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jBAlterar)
                        .addGap(18, 18, 18)
                        .addComponent(jBExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jBSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jBCancelar)))
                .addContainerGap(89, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFCodigo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTFSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBCTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 142, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBNovo)
                    .addComponent(jBBuscar)
                    .addComponent(jBAlterar)
                    .addComponent(jBExcluir)
                    .addComponent(jBSalvar)
                    .addComponent(jBCancelar))
                .addContainerGap())
        );

        getAccessibleContext().setAccessibleDescription("");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jTFCodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFCodigoActionPerformed
       
    }//GEN-LAST:event_jTFCodigoActionPerformed

    private void jTFNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFNomeActionPerformed
      
    }//GEN-LAST:event_jTFNomeActionPerformed

    private void jBSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSalvarActionPerformed
        Usuario usuario = new Usuario();
        UsuarioDAO udao = new UsuarioDAO();
        boolean verifica = true;
        
        for (Usuario u : udao.findAll()) {
            if(u.getLogin().equals(jTFLogin.getText())){
                if(jTFCodigo.getText().equals("")){
                    verifica = false;
                    break;
                }
            }
        }
        
        if(!verifica)
            JOptionPane.showMessageDialog(this, " Login já existente!");
        
        if(verifica){
            if(jTFSenha.getText().length() <6)
                JOptionPane.showMessageDialog(this, " A senha deve conter 6 caracteres ou mais!");
            else{
                usuario.setNome(jTFNome.getText());
                usuario.setLogin(jTFLogin.getText());
                usuario.setSenha(criptografar(jTFSenha.getText()));

                if(jBCTipo.getSelectedItem().equals("Administrador"))
                    usuario.setTipo(1);
                else 
                    usuario.setTipo(2);
                
                if(jTFCodigo.getText().equals(""))    
                    UsuarioDAO.getInstance().persist(usuario);
                else{
                    usuario.setCodigo(Integer.parseInt(jTFCodigo.getText()));
                    UsuarioDAO.getInstance().merge(usuario);
                }

                JOptionPane.showMessageDialog(this, " Salvo com sucesso!");    
                inicio();
            }
        }    
    }//GEN-LAST:event_jBSalvarActionPerformed

    private void jBBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarActionPerformed
        desabilitarCampos();
        FContexto.getListarUsuarios().setVisible(true);
        FContexto.getListarUsuarios().readJTable();
    }//GEN-LAST:event_jBBuscarActionPerformed
    
    public void setUsuario(Usuario usuario){
        jTFCodigo.setText(String.valueOf(usuario.getCodigo()));
        jTFNome.setText(usuario.getNome());
        jTFLogin.setText(usuario.getLogin());
        jTFSenha.setText("");
        if(usuario.getTipo() == 1)
            jBCTipo.setSelectedItem("Administrador");
        else
            jBCTipo.setSelectedItem("Usuário");
        jBExcluir.setEnabled(true);
        jBAlterar.setEnabled(true);
        jBCancelar.setEnabled(true);
        jBSalvar.setEnabled(false);
    }
    
    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        int codigo = Integer.parseInt(jTFCodigo.getText());
        UsuarioDAO.getInstance().removeById(codigo);
        JOptionPane.showMessageDialog(this, " Usuário excluído com sucesso!");
        inicio();
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jTFSenhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFSenhaActionPerformed
    
    }//GEN-LAST:event_jTFSenhaActionPerformed

    private void jBCTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCTipoActionPerformed
        
    }//GEN-LAST:event_jBCTipoActionPerformed

    private void jTFNomeInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jTFNomeInputMethodTextChanged
       
    }//GEN-LAST:event_jTFNomeInputMethodTextChanged

    private void jTFNomePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTFNomePropertyChange
    
    }//GEN-LAST:event_jTFNomePropertyChange

    private void jTFNomeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFNomeKeyReleased
        if(verificaCampos())
            jBSalvar.setEnabled(true);
        else 
            jBSalvar.setEnabled(false);
    }//GEN-LAST:event_jTFNomeKeyReleased

    private void jTFLoginKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFLoginKeyReleased
        if(verificaCampos())
            jBSalvar.setEnabled(true);
        else 
            jBSalvar.setEnabled(false);
    }//GEN-LAST:event_jTFLoginKeyReleased

    private void jTFSenhaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFSenhaKeyReleased
        if(verificaCampos())
            jBSalvar.setEnabled(true);
        else 
            jBSalvar.setEnabled(false);
    }//GEN-LAST:event_jTFSenhaKeyReleased

    private void jBNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBNovoActionPerformed
        limpar();
        habilitarCampos();
        jBCancelar.setEnabled(true);
        jBNovo.setEnabled(false);
    }//GEN-LAST:event_jBNovoActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
        habilitarCampos();
        jBAlterar.setEnabled(false);
        jBBuscar.setEnabled(false);
        jBSalvar.setEnabled(false);
        jBNovo.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBCancelar.setEnabled(true);
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        inicio();
    }//GEN-LAST:event_jBCancelarActionPerformed
    
    private void inicio(){
        limpar();
        desabilitarCampos();
        jBNovo.setEnabled(true);
        jBBuscar.setEnabled(true);
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBSalvar.setEnabled(false);
        jBCancelar.setEnabled(false);
    }
    
    private void desabilitarCampos(){
        jTFNome.setEnabled(false);
        jTFLogin.setEnabled(false);
        jTFSenha.setEnabled(false);
        jBCTipo.setEnabled(false);
    }
    
    private void habilitarCampos(){
        jTFNome.setEnabled(true);
        jTFLogin.setEnabled(true);
        jTFSenha.setEnabled(true);
        jBCTipo.setEnabled(true);
    }
  
    private void limpar(){
        jTFCodigo.setText("");
        jTFLogin.setText("");
        jTFNome.setText("");
        jTFSenha.setText("");
    }
    
    public boolean verificaCampos(){
        boolean resp = true;
        
        if(jTFNome.getText().equals(""))
            resp = false;
        if(jTFLogin.getText().equals(""))
            resp = false;
        if(jTFSenha.getText().equals(""))
            resp = false;
        
        return resp;
    }

    public static void main(String args[]) {
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFTelaUsuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFTelaUsuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFTelaUsuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFTelaUsuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFTelaUsuarios().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jBAlterar;
    public javax.swing.JButton jBBuscar;
    private javax.swing.JComboBox<String> jBCTipo;
    public javax.swing.JButton jBCancelar;
    public javax.swing.JButton jBExcluir;
    public javax.swing.JButton jBNovo;
    public javax.swing.JButton jBSalvar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JTextField jTFCodigo;
    private javax.swing.JTextField jTFLogin;
    private javax.swing.JTextField jTFNome;
    private javax.swing.JPasswordField jTFSenha;
    // End of variables declaration//GEN-END:variables
}
