package Apresentação;

import DataAccess.FornecedorDAO;
import DataAccess.MercadoriaFornecedorDAO;
import DomainModel.FContexto;
import DomainModel.Fornecedor;
import DomainModel.MercadoriaFornecedor;
import static Validações.validaCNPJ.isCNPJ;
import static Validações.validaCPF.isCPF;
import static Validações.validaEmail.isEmail;
import javax.swing.JOptionPane;

//@author marcelo

public class JFTelaFornecedores extends javax.swing.JFrame {

    public JFTelaFornecedores() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPasswordField1 = new javax.swing.JPasswordField();
        jTFCodigo = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTFNome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTFLogradouro = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTFCidade = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jTFBairro = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jCBUf = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        jTFEmail = new javax.swing.JTextField();
        jBNovo = new javax.swing.JButton();
        jBBuscar = new javax.swing.JButton();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBSalvar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();
        jTFCnpj = new javax.swing.JFormattedTextField();
        jTFTelefone = new javax.swing.JFormattedTextField();
        jLabel11 = new javax.swing.JLabel();
        jTFCep = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        jTFCpf = new javax.swing.JFormattedTextField();
        jCBTipo = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();

        jPasswordField1.setText("jPasswordField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tela de Fornecedores");
        setBackground(new java.awt.Color(255, 255, 255));
        setSize(new java.awt.Dimension(0, 0));

        jTFCodigo.setEditable(false);
        jTFCodigo.setEnabled(false);

        jLabel1.setText("Código");

        jLabel2.setText("Nome");

        jTFNome.setEnabled(false);
        jTFNome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFNomeKeyReleased(evt);
            }
        });

        jLabel3.setText("CNPJ");

        jLabel5.setText("Telefone");

        jLabel6.setText("Endereço");

        jTFLogradouro.setEnabled(false);
        jTFLogradouro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFLogradouroKeyReleased(evt);
            }
        });

        jLabel7.setText("Cidade");

        jTFCidade.setEnabled(false);
        jTFCidade.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFCidadeKeyReleased(evt);
            }
        });

        jLabel8.setText("Bairro");

        jTFBairro.setEnabled(false);
        jTFBairro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFBairroKeyReleased(evt);
            }
        });

        jLabel9.setText("UF");

        jCBUf.setEditable(true);
        jCBUf.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO" }));
        jCBUf.setEnabled(false);

        jLabel10.setText("E-mail");

        jTFEmail.setEnabled(false);

        jBNovo.setText("Novo");
        jBNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBNovoActionPerformed(evt);
            }
        });

        jBBuscar.setText("Buscar");
        jBBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarActionPerformed(evt);
            }
        });

        jBAlterar.setText("Alterar");
        jBAlterar.setEnabled(false);
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });

        jBExcluir.setText("Excluir");
        jBExcluir.setEnabled(false);
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });

        jBSalvar.setText("Salvar");
        jBSalvar.setEnabled(false);
        jBSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSalvarActionPerformed(evt);
            }
        });

        jBCancelar.setText("Cancelar");
        jBCancelar.setEnabled(false);
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });

        try {
            jTFCnpj.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###/####-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jTFCnpj.setEnabled(false);
        jTFCnpj.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFCnpjKeyReleased(evt);
            }
        });

        try {
            jTFTelefone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)#####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jTFTelefone.setEnabled(false);
        jTFTelefone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFTelefoneKeyReleased(evt);
            }
        });

        jLabel11.setText("CEP");

        try {
            jTFCep.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#####-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jTFCep.setEnabled(false);
        jTFCep.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFCepKeyReleased(evt);
            }
        });

        jLabel4.setText("CPF");

        try {
            jTFCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jTFCpf.setEnabled(false);
        jTFCpf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFCpfKeyReleased(evt);
            }
        });

        jCBTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Jurídica", "Física" }));
        jCBTipo.setAlignmentX(1.0F);
        jCBTipo.setAlignmentY(1.0F);
        jCBTipo.setEnabled(false);
        jCBTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBTipoActionPerformed(evt);
            }
        });

        jLabel12.setText("Tipo");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(12, 12, 12)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTFNome, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(layout.createSequentialGroup()
                                                        .addComponent(jTFCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(jTFCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                    .addComponent(jTFLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(12, 12, 12)
                                                .addComponent(jLabel12)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jCBTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jTFTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel10)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jTFEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 318, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel11)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jTFCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel8)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jTFCep, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel9)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jCBUf, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(jTFBairro)))))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(jBNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jBBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jBAlterar)
                        .addGap(18, 18, 18)
                        .addComponent(jBExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jBSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jBCancelar)))
                .addGap(68, 68, 68))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFCodigo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTFNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTFCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jTFCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCBTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTFTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(jTFEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTFLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jTFCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(jTFBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel9)
                        .addComponent(jCBUf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTFCep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 68, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBNovo)
                    .addComponent(jBBuscar)
                    .addComponent(jBAlterar)
                    .addComponent(jBExcluir)
                    .addComponent(jBSalvar)
                    .addComponent(jBCancelar))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSalvarActionPerformed
        Fornecedor fornecedor = new Fornecedor();
        FornecedorDAO fdao = new FornecedorDAO();
        boolean valido = true;
        
        for (Fornecedor f : fdao.findAll()) {
            if(f.getCnpj().equals(jTFCnpj.getText())){
                if(jTFCodigo.getText().equals("")){
                    valido = false;
                    break;
                }
            }
        }
        
        if(!valido&&jCBTipo.getSelectedItem().equals("Jurídica"))
            JOptionPane.showMessageDialog(this, " CNPJ já existente!");
        
        for (Fornecedor f : fdao.findAll()) {
            if(f.getCpf().equals(jTFCpf.getText())){
                if(jTFCodigo.getText().equals("")){
                    valido = false;
                    break;
                }
            }
        }
        
        if(!valido&&jCBTipo.getSelectedItem().equals("Física"))
            JOptionPane.showMessageDialog(this, " CPF já existente!");
        
        if(!isCNPJ(jTFCnpj.getText())&&jCBTipo.getSelectedItem().equals("Jurídica")){
            valido = false;
            JOptionPane.showMessageDialog(this, " CNPJ Inválido!");
        }       
        
        if(!isEmail(jTFEmail.getText()) && !jTFEmail.getText().equals("")){
            JOptionPane.showMessageDialog(this, " E-mail Inválido!");
            valido = false;
        } 
        
        if(!isCPF(jTFCpf.getText()) && jCBTipo.getSelectedItem().equals("Física")){
            JOptionPane.showMessageDialog(this, " CPF Inválido!");
            valido = false;
        } 
        
        if(valido){
  
            if(jCBTipo.getSelectedItem().equals("Jurídica"))
                fornecedor.setTipo(1);
            else 
                fornecedor.setTipo(2);
            
            if(jCBTipo.getSelectedItem().equals("Jurídica")){
                fornecedor.setCnpj(jTFCnpj.getText());
                fornecedor.setCpf("");
            }
            
            if(jCBTipo.getSelectedItem().equals("Física")){
                fornecedor.setCpf(jTFCpf.getText());
                fornecedor.setCnpj("");
            }    
            
            fornecedor.setNome(jTFNome.getText());
            fornecedor.setTelefone(jTFTelefone.getText());
            fornecedor.setLogradouro(jTFLogradouro.getText());
            fornecedor.setBairro(jTFBairro.getText());
            fornecedor.setCidade(jTFCidade.getText());
            fornecedor.setCep(jTFCep.getText());
            fornecedor.setUf((String) jCBUf.getSelectedItem());
            fornecedor.setEmail(jTFEmail.getText());

            if(jTFCodigo.getText().equals(""))    
                FornecedorDAO.getInstance().persist(fornecedor);
            else{
                fornecedor.setCodigo(Integer.parseInt(jTFCodigo.getText()));
                FornecedorDAO.getInstance().merge(fornecedor);
            }

            JOptionPane.showMessageDialog(this, " Salvo com sucesso!");
            inicio();
        }    
    }//GEN-LAST:event_jBSalvarActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        MercadoriaFornecedorDAO mfdao = new MercadoriaFornecedorDAO();
        int codigo = Integer.parseInt(jTFCodigo.getText());
        FornecedorDAO.getInstance().removeById(codigo);
        
        for (MercadoriaFornecedor mf : mfdao.findAll()) {
            if(mf.getIdFornecedor() == codigo)
                mfdao.removeById(mf.getIdMercadoria());
        }    

        JOptionPane.showMessageDialog(this, " Fornecedor excluído com sucesso!");
        inicio();
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        inicio();
    }//GEN-LAST:event_jBCancelarActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
        habilitarCampos();
        jBAlterar.setEnabled(false);
        jBBuscar.setEnabled(false);
        jBSalvar.setEnabled(true);
        jBNovo.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBCancelar.setEnabled(true);
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBNovoActionPerformed
        limpar();
        habilitarCampos();
        jBCancelar.setEnabled(true);
        jBNovo.setEnabled(false);
    }//GEN-LAST:event_jBNovoActionPerformed

    private void jTFNomeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFNomeKeyReleased
        if(verificaCampos())
            jBSalvar.setEnabled(true);
        else 
            jBSalvar.setEnabled(false);
    }//GEN-LAST:event_jTFNomeKeyReleased

    private void jTFCnpjKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFCnpjKeyReleased
        if(verificaCampos())
            jBSalvar.setEnabled(true);
        else 
            jBSalvar.setEnabled(false);
    }//GEN-LAST:event_jTFCnpjKeyReleased

    private void jTFTelefoneKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFTelefoneKeyReleased
        if(verificaCampos())
            jBSalvar.setEnabled(true);
        else 
            jBSalvar.setEnabled(false);
    }//GEN-LAST:event_jTFTelefoneKeyReleased

    private void jTFLogradouroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFLogradouroKeyReleased
        if(verificaCampos())
            jBSalvar.setEnabled(true);
        else 
            jBSalvar.setEnabled(false);
    }//GEN-LAST:event_jTFLogradouroKeyReleased

    private void jTFBairroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFBairroKeyReleased
        if(verificaCampos())
            jBSalvar.setEnabled(true);
        else 
            jBSalvar.setEnabled(false);
    }//GEN-LAST:event_jTFBairroKeyReleased

    private void jTFCidadeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFCidadeKeyReleased
        if(verificaCampos())
            jBSalvar.setEnabled(true);
        else 
            jBSalvar.setEnabled(false);
    }//GEN-LAST:event_jTFCidadeKeyReleased

    private void jTFCepKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFCepKeyReleased
        if(verificaCampos())
            jBSalvar.setEnabled(true);
        else 
            jBSalvar.setEnabled(false);
    }//GEN-LAST:event_jTFCepKeyReleased

    private void jBBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarActionPerformed
        FContexto.getListarFornecedores().setVisible(true);
        FContexto.getListarFornecedores().readJTable();
    }//GEN-LAST:event_jBBuscarActionPerformed

    private void jCBTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBTipoActionPerformed
        if(jCBTipo.getSelectedItem().equals("Jurídica")){
            jTFCnpj.setEnabled(true);
            jTFCpf.setEnabled(false);
            jTFCpf.setText("");
        }else{ 
            jTFCpf.setEnabled(true);
            jTFCnpj.setEnabled(false);
            jTFCnpj.setText("");
        }    
    }//GEN-LAST:event_jCBTipoActionPerformed

    private void jTFCpfKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFCpfKeyReleased
        if(verificaCampos())
            jBSalvar.setEnabled(true);
        else 
            jBSalvar.setEnabled(false);
    }//GEN-LAST:event_jTFCpfKeyReleased
    
    public void setFornecedor(Fornecedor fornecedor){
        jTFCodigo.setText(String.valueOf(fornecedor.getCodigo()));
        jTFNome.setText(fornecedor.getNome());
        jTFCnpj.setText(fornecedor.getCnpj());
        jTFTelefone.setText(fornecedor.getTelefone());
        jTFLogradouro.setText(fornecedor.getLogradouro());
        jTFBairro.setText(fornecedor.getBairro());
        jTFCidade.setText(fornecedor.getCidade());
        jTFCep.setText(fornecedor.getCep());
        jCBUf.setSelectedItem(fornecedor.getUf());
        jTFEmail.setText(fornecedor.getEmail());
        jTFCpf.setText(fornecedor.getCpf());
        
        if(fornecedor.getTipo() == 1)
            jCBTipo.setSelectedItem("Jurídica");
        else
            jCBTipo.setSelectedItem("Física");
        
        jBExcluir.setEnabled(true);
        jBAlterar.setEnabled(true);
        jBCancelar.setEnabled(true);
        jBSalvar.setEnabled(false);
    }
    
    private void inicio(){
        limpar();
        desabilitarCampos();
        jBNovo.setEnabled(true);
        jBBuscar.setEnabled(true);
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBSalvar.setEnabled(false);
        jBCancelar.setEnabled(false);
    }
    
    void desabilitarCampos(){
        jTFNome.setEnabled(false);
        jTFCnpj.setEnabled(false);
        jTFTelefone.setEnabled(false);
        jTFLogradouro.setEnabled(false);
        jTFBairro.setEnabled(false);
        jTFCidade.setEnabled(false);
        jTFCep.setEnabled(false);
        jCBUf.setEnabled(false);
        jTFEmail.setEnabled(false);
        jCBTipo.setEnabled(false);
        jTFCpf.setEnabled(false);
    }
    
    private void habilitarCampos(){
        jTFNome.setEnabled(true);
        jTFTelefone.setEnabled(true);
        jTFLogradouro.setEnabled(true);
        jTFBairro.setEnabled(true);
        jTFCidade.setEnabled(true);
        jTFCep.setEnabled(true);
        jCBUf.setEnabled(true);
        jTFEmail.setEnabled(true);
        jCBTipo.setEnabled(true);
        
        if(jCBTipo.getSelectedItem().equals("Jurídica"))
            jTFCnpj.setEnabled(true);
        else 
            jTFCpf.setEnabled(true);
    }
  
    private void limpar(){
        jTFCodigo.setText("");
        jTFNome.setText("");
        jTFCnpj.setText("");
        jTFTelefone.setText("");
        jTFLogradouro.setText("");
        jTFBairro.setText("");
        jTFCidade.setText("");
        jTFCep.setText("");
        jTFEmail.setText("");
        jTFCpf.setText("");
    }
    
    public boolean verificaCampos(){
        boolean resp = true;
        
        if(jTFNome.getText().equals(""))
            resp = false;
        if(jTFCnpj.getText().equals("  .   .   /    -  ")&&jCBTipo.getSelectedItem().equals("Jurídica"))
            resp = false;
        if(jTFTelefone.getText().equals("(  )     -    "))
            resp = false;
        if(jTFLogradouro.getText().equals(""))
            resp = false;
        if(jTFBairro.getText().equals(""))
            resp = false;
        if(jTFCidade.getText().equals(""))
            resp = false;
        if(jTFCep.getText().equals("     -   "))
            resp = false;
        if(jTFCpf.getText().equals("   .   .   -  ")&&jCBTipo.getSelectedItem().equals("Física"))
            resp = false;
        
        return resp;
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFTelaFornecedores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFTelaFornecedores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFTelaFornecedores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFTelaFornecedores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFTelaFornecedores().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jBAlterar;
    public javax.swing.JButton jBBuscar;
    public javax.swing.JButton jBCancelar;
    public javax.swing.JButton jBExcluir;
    public javax.swing.JButton jBNovo;
    public javax.swing.JButton jBSalvar;
    private javax.swing.JComboBox<String> jCBTipo;
    private javax.swing.JComboBox<String> jCBUf;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JTextField jTFBairro;
    private javax.swing.JFormattedTextField jTFCep;
    private javax.swing.JTextField jTFCidade;
    private javax.swing.JFormattedTextField jTFCnpj;
    private javax.swing.JTextField jTFCodigo;
    private javax.swing.JFormattedTextField jTFCpf;
    private javax.swing.JTextField jTFEmail;
    private javax.swing.JTextField jTFLogradouro;
    private javax.swing.JTextField jTFNome;
    private javax.swing.JFormattedTextField jTFTelefone;
    // End of variables declaration//GEN-END:variables
}
