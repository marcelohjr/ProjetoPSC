package Apresentação;

import DataAccess.RetiradasDAO;
import DataAccess.VendaDAO;
import DomainModel.Retiradas;
import DomainModel.Venda;
import static Validações.validaData.compareData;
import static Validações.validaData.isData;
import static Validações.validaData.isDataMenorHoje;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class JFRelatorioFinanceiro extends javax.swing.JFrame {

    public JFRelatorioFinanceiro() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTBalanco = new javax.swing.JTable();
        jDCDataFim = new com.toedter.calendar.JDateChooser();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jDCDataInicio = new com.toedter.calendar.JDateChooser();
        jBFiltrar = new javax.swing.JButton();
        jBListar = new javax.swing.JButton();
        jBFechar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jTFBalanco = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Financeiro");
        jLabel2.setToolTipText("");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTBalanco.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Data", "Operação", "Valor"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTBalanco.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(jTBalanco);
        if (jTBalanco.getColumnModel().getColumnCount() > 0) {
            jTBalanco.getColumnModel().getColumn(0).setResizable(false);
            jTBalanco.getColumnModel().getColumn(1).setResizable(false);
            jTBalanco.getColumnModel().getColumn(2).setResizable(false);
        }

        jLabel5.setText("Data Fim");

        jLabel1.setText("Data Início");

        jBFiltrar.setText("Filtrar");
        jBFiltrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBFiltrarActionPerformed(evt);
            }
        });

        jBListar.setText("Listar Todos");
        jBListar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBListarActionPerformed(evt);
            }
        });

        jBFechar.setText("Fechar");
        jBFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBFecharActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Balanço: ");

        jTFBalanco.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTFBalanco.setText("jLabel4");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(299, 299, 299)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jBFiltrar)
                                .addGap(130, 130, 130))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(70, 70, 70)
                                .addComponent(jBListar))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(349, 349, 349)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTFBalanco)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jBFechar))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jDCDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jDCDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(96, 96, 96)))))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 324, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(364, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDCDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDCDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(25, 25, 25)
                .addComponent(jBFiltrar)
                .addGap(18, 18, 18)
                .addComponent(jBListar)
                .addGap(133, 133, 133)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBFechar)
                    .addComponent(jLabel3)
                    .addComponent(jTFBalanco))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(51, 51, 51)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBFiltrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBFiltrarActionPerformed
        boolean valido = true;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dataInicio = formatter.format(jDCDataInicio.getDate());
        String dataFim = formatter.format(jDCDataFim.getDate());

        if (!isData(dataInicio) || !isDataMenorHoje(dataInicio)) {
            valido = false;
            JOptionPane.showMessageDialog(this, " Data de Início Inválida!");
        }

        if (!isData(dataFim) || !isDataMenorHoje(dataFim)) {
            valido = false;
            JOptionPane.showMessageDialog(this, " Data Final Inválida!");
        }

        if (valido) {
            try {
                if (compareData(dataInicio, dataFim) > 0) {
                    valido = false;
                    JOptionPane.showMessageDialog(this, " A Data de Início não pode ser maior que a Data Final!");
                }
            } catch (ParseException ex) {
                Logger.getLogger(JFTelaNotaFiscal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (valido) {
            readJTableForDate();
        }
    }//GEN-LAST:event_jBFiltrarActionPerformed

    private void jBListarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBListarActionPerformed
        readJTable();
    }//GEN-LAST:event_jBListarActionPerformed

    private void jBFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBFecharActionPerformed
        dispose();
    }//GEN-LAST:event_jBFecharActionPerformed

    public void readJTable() {
        ((JTextField) jDCDataInicio.getDateEditor()).setEditable(false);
        ((JTextField) jDCDataFim.getDateEditor()).setEditable(false);
        DefaultTableModel modelo = (DefaultTableModel) jTBalanco.getModel();
        modelo.setNumRows(0);
        VendaDAO vdao = new VendaDAO();
        RetiradasDAO rdao = new RetiradasDAO();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String data;
        float balanco = (float) 0.0;

        for (Venda v : vdao.findAll()) {

            data = formatter.format(v.getData());
            balanco += v.getValorTotal();

            modelo.addRow(new Object[]{
                data,
                "Entrada",
                v.getValorTotal()
            });
        }

        for (Retiradas r : rdao.findAll()) {

            data = formatter.format(r.getData());
            balanco -= r.getValor();

            modelo.addRow(new Object[]{
                data,
                "Saída",
                r.getValor()
            });
        }

        jTFBalanco.setText(String.valueOf(balanco));
    }

    public void readJTableForDate() {

        DefaultTableModel modelo = (DefaultTableModel) jTBalanco.getModel();
        modelo.setNumRows(0);
        VendaDAO vdao = new VendaDAO();
        RetiradasDAO rdao = new RetiradasDAO();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dataInicio = formatter.format(jDCDataInicio.getDate());
        String dataFim = formatter.format(jDCDataFim.getDate());
        String data;
        float balanco = (float) 0.0;

        for (Venda v : vdao.findAll()) {

            data = formatter.format(v.getData());

            try {
                if (compareData(data, dataInicio) >= 0 && compareData(data, dataFim) <= 0) {
                    balanco += v.getValorTotal();
                    modelo.addRow(new Object[]{
                        data,
                        "Entrada",
                        v.getValorTotal()
                    });
                }
            } catch (ParseException ex) {
                Logger.getLogger(JFTelaRetiradas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        for (Retiradas r : rdao.findAll()) {

            data = formatter.format(r.getData());

            try {
                if (compareData(data, dataInicio) >= 0 && compareData(data, dataFim) <= 0) {
                    balanco -= r.getValor();
                    modelo.addRow(new Object[]{
                        data,
                        "Saída",
                        r.getValor()
                    });
                }
            } catch (ParseException ex) {
                Logger.getLogger(JFTelaRetiradas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        jTFBalanco.setText(String.valueOf(balanco));
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFRelatorioFinanceiro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFRelatorioFinanceiro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFRelatorioFinanceiro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFRelatorioFinanceiro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFRelatorioFinanceiro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBFechar;
    private javax.swing.JButton jBFiltrar;
    private javax.swing.JButton jBListar;
    private com.toedter.calendar.JDateChooser jDCDataFim;
    private com.toedter.calendar.JDateChooser jDCDataInicio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTBalanco;
    private javax.swing.JLabel jTFBalanco;
    // End of variables declaration//GEN-END:variables
}
