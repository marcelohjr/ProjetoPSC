package Apresentação;

import DataAccess.CaixaDAO;
import DataAccess.MercadoriaDAO;
import DataAccess.VendaDAO;
import DataAccess.VendaMercadoriaDAO;
import DomainModel.Caixa;
import DomainModel.FContexto;
import DomainModel.Mercadoria;
import DomainModel.Venda;
import DomainModel.VendaMercadoria;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class JFTelaVendas extends javax.swing.JFrame {

    public JFTelaVendas() {
        initComponents();
        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        jTProdutos.setRowSorter(new TableRowSorter(modelo));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTFCodigo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTFData = new javax.swing.JFormattedTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTProdutos = new javax.swing.JTable();
        jBAdicionar = new javax.swing.JButton();
        jBRemover = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jTFValorTotal = new javax.swing.JFormattedTextField();
        jLabel12 = new javax.swing.JLabel();
        jBNova = new javax.swing.JButton();
        jBFinalizar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Código");

        jTFCodigo.setEnabled(false);

        jLabel2.setText("Data da Venda");

        jTFData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));
        jTFData.setEnabled(false);

        jSeparator1.setToolTipText("");
        jSeparator1.setName("Produtos"); // NOI18N

        jLabel3.setText("Produtos");

        jTProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Descrição", "Preço Unitário", "Quantidade", "Preço Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Float.class, java.lang.Integer.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTProdutos.setEnabled(false);
        jTProdutos.getTableHeader().setReorderingAllowed(false);
        jTProdutos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTProdutosMouseClicked(evt);
            }
        });
        jTProdutos.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTProdutosPropertyChange(evt);
            }
        });
        jScrollPane2.setViewportView(jTProdutos);

        jBAdicionar.setText("Adicionar");
        jBAdicionar.setEnabled(false);
        jBAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAdicionarActionPerformed(evt);
            }
        });

        jBRemover.setText("Remover");
        jBRemover.setEnabled(false);
        jBRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBRemoverActionPerformed(evt);
            }
        });

        jTFValorTotal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));
        jTFValorTotal.setEnabled(false);
        jTFValorTotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTFValorTotalKeyTyped(evt);
            }
        });

        jLabel12.setText("Valor Total");

        jBNova.setText("Nova");
        jBNova.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBNovaActionPerformed(evt);
            }
        });

        jBFinalizar.setText("Finalizar");
        jBFinalizar.setEnabled(false);
        jBFinalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBFinalizarActionPerformed(evt);
            }
        });

        jBCancelar.setText("Cancelar");
        jBCancelar.setEnabled(false);
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(63, 63, 63)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTFData, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(194, 194, 194)
                                .addComponent(jBAdicionar)
                                .addGap(123, 123, 123)
                                .addComponent(jBRemover))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(304, 304, 304)
                                .addComponent(jLabel3)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 642, Short.MAX_VALUE)
                            .addComponent(jSeparator2)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jBNova, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jBFinalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jBCancelar)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTFValorTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jTFData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBAdicionar)
                    .addComponent(jBRemover))
                .addGap(18, 18, 18)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFValorTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBNova)
                    .addComponent(jBFinalizar)
                    .addComponent(jBCancelar))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jTProdutosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTProdutosMouseClicked
        if (jTProdutos.getSelectedRow() != -1) {
            jBRemover.setEnabled(true);
        }
    }//GEN-LAST:event_jTProdutosMouseClicked

    private void jTProdutosPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTProdutosPropertyChange
        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        int size = modelo.getRowCount(), qt = 0, codigo = 0;

        if (jTProdutos.getSelectedRow() >= 0) {
            qt = (int) jTProdutos.getModel().getValueAt(jTProdutos.getSelectedRow(), 3);
            codigo = (int) jTProdutos.getModel().getValueAt(jTProdutos.getSelectedRow(), 0);
        }

        float vu, valorTotal = (float) 0.0;
        Mercadoria mercadoria = new Mercadoria();
        MercadoriaDAO mdao = new MercadoriaDAO();

        for (Mercadoria m : mdao.findAll()) {
            if (codigo == m.getCodigo()) {
                mercadoria = m;
                break;
            }
        }

        if (mercadoria.getEstoqueAtual() < qt) {
            JOptionPane.showMessageDialog(this, " Quantidade indisponível em estoque!");
            jTProdutos.getModel().setValueAt(mercadoria.getEstoqueAtual(), jTProdutos.getSelectedRow(), 3);
        }

        for (int i = 0; i < size; i++) {
            qt = (int) jTProdutos.getModel().getValueAt(i, 3);
            vu = (float) jTProdutos.getModel().getValueAt(i, 2);

            jTProdutos.getModel().setValueAt((qt * vu), i, 4);
        }

        for (int i = 0; i < size; i++) {
            valorTotal += (float) jTProdutos.getModel().getValueAt(i, 4);
        }

        String valor = String.valueOf(valorTotal);
        valor = valor.replace(".", ",");
        jTFValorTotal.setText(String.valueOf(valor));
    }//GEN-LAST:event_jTProdutosPropertyChange

    private void jBAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAdicionarActionPerformed
        FContexto.getListarMercadorias().setVisible(true);
    }//GEN-LAST:event_jBAdicionarActionPerformed

    private void jBRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBRemoverActionPerformed
        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        if (jTProdutos.getSelectedRow() >= 0) {
            modelo.removeRow(jTProdutos.getSelectedRow());
            jTProdutos.setModel(modelo);
        }

        int size = modelo.getRowCount();
        float valorTotal = (float) 0.0;

        for (int i = 0; i < size; i++) {
            valorTotal += (float) jTProdutos.getModel().getValueAt(i, 4);
        }

        String valor = String.valueOf(valorTotal);
        valor = valor.replace(".", ",");
        jTFValorTotal.setText(String.valueOf(valor));

        if (verificaCampos()) {
            jBFinalizar.setEnabled(true);
        } else {
            jBFinalizar.setEnabled(false);
        }
    }//GEN-LAST:event_jBRemoverActionPerformed

    private void jTFValorTotalKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFValorTotalKeyTyped
        String caracteres = "0987654321,";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_jTFValorTotalKeyTyped

    private void jBNovaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBNovaActionPerformed
        String data;
        limpar();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        data = formatter.format(new Date());
        jTFData.setText(data);
        habilitarCampos();
        jBCancelar.setEnabled(true);
        jBNova.setEnabled(false);
    }//GEN-LAST:event_jBNovaActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        inicio();
    }//GEN-LAST:event_jBCancelarActionPerformed

    private void jBFinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBFinalizarActionPerformed
        Venda venda = new Venda();
        VendaMercadoria vendaMercadoria = new VendaMercadoria();
        Mercadoria mercadoria = new Mercadoria();
        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        int size = modelo.getRowCount(), codigo;

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date data = null;

        try {
            data = formato.parse(jTFData.getText());
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaMercadorias.class.getName()).log(Level.SEVERE, null, ex);
        }

        venda.setData(data);

        String valorTotal = jTFValorTotal.getText();
        valorTotal = valorTotal.replace(".", "");
        valorTotal = valorTotal.replace(",", ".");

        venda.setValorTotal(Float.parseFloat(valorTotal));
        venda.setCodigo(VendaDAO.getInstance().merge(venda));
        
        for(int i = 0; i < size; i++){
            vendaMercadoria = new VendaMercadoria();
            
            codigo = (int) jTProdutos.getModel().getValueAt(i, 0);
            
            mercadoria = MercadoriaDAO.getInstance().getById(codigo);
            vendaMercadoria.setIdMercadoria(mercadoria.getCodigo());
            vendaMercadoria.setIdVenda(venda.getCodigo());
            vendaMercadoria.setDescricao(mercadoria.getNome());
            vendaMercadoria.setPrecoUnitario(mercadoria.getPrecoVenda());
            vendaMercadoria.setQuantidade((int) jTProdutos.getModel().getValueAt(i, 3));
            
            mercadoria.setEstoqueAtual(mercadoria.getEstoqueAtual() - (int) jTProdutos.getModel().getValueAt(i, 3));
            
            MercadoriaDAO.getInstance().merge(mercadoria);
            VendaMercadoriaDAO.getInstance().persist(vendaMercadoria);
        }
        
        Caixa caixa = new Caixa();
        CaixaDAO cdao = new CaixaDAO();
        formato = new SimpleDateFormat("yyyy/MM/dd");
        String dataCaixa = formato.format(new Date());
        Date dataHoje = null;

        try {
            dataHoje = formato.parse(dataCaixa);
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaMercadorias.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Caixa cx : cdao.findAll()) {
            if (dataHoje.compareTo(cx.getData()) == 0) {
                caixa = cx;
                break;
            }
        }
        
        caixa.setTotalVendas(caixa.getTotalVendas() + Float.parseFloat(valorTotal));
        caixa.setSaldoFinal(caixa.getTotalVendas() + caixa.getSaldoInicial());
        CaixaDAO.getInstance().merge(caixa);
        
        JOptionPane.showMessageDialog(this, " Venda realizada com sucesso!");
        inicio();
    }//GEN-LAST:event_jBFinalizarActionPerformed

    public void setMercadoria(Mercadoria m) {
        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        int size = modelo.getRowCount();
        boolean valido = true;
        int codigo;
        float valorTotal = (float) 0.0;

        for (int i = 0; i < size; i++) {
            codigo = (int) jTProdutos.getModel().getValueAt(i, 0);
            if (m.getCodigo() == codigo) {
                valido = false;
            }
        }

        if (valido) {
            modelo.addRow(new Object[]{
                m.getCodigo(),
                m.getNome(),
                m.getPrecoVenda(),
                1,
                m.getPrecoVenda()
            });
        }

        size = modelo.getRowCount();

        for (int i = 0; i < size; i++) {
            valorTotal += (float) jTProdutos.getModel().getValueAt(i, 4);
        }

        String valor = String.valueOf(valorTotal);
        valor = valor.replace(".", ",");
        jTFValorTotal.setText(valor);

        if (verificaCampos()) {
            jBFinalizar.setEnabled(true);
        } else {
            jBFinalizar.setEnabled(false);
        }
    }

    private void inicio() {
        limpar();
        desabilitarCampos();
        jBNova.setEnabled(true);
        jBFinalizar.setEnabled(false);
        jBCancelar.setEnabled(false);
    }

    void desabilitarCampos() {
        jTProdutos.setEnabled(false);
        jBCancelar.setEnabled(true);
        jBAdicionar.setEnabled(false);
        jBRemover.setEnabled(false);
    }

    private void habilitarCampos() {
        jTProdutos.setEnabled(true);
        jBCancelar.setEnabled(true);
        jBAdicionar.setEnabled(true);
    }

    private void limpar() {
        jTFCodigo.setText("");
        jTFData.setText("");
        jTFValorTotal.setText("");

        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        modelo.setNumRows(0);
    }

    public boolean verificaCampos() {
        boolean resp = true;

        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        int size = modelo.getRowCount();

        if (size < 1) {
            resp = false;
        }

        return resp;
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFTelaVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFTelaVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFTelaVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFTelaVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFTelaVendas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBAdicionar;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBFinalizar;
    private javax.swing.JButton jBNova;
    private javax.swing.JButton jBRemover;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField jTFCodigo;
    private javax.swing.JFormattedTextField jTFData;
    private javax.swing.JFormattedTextField jTFValorTotal;
    private javax.swing.JTable jTProdutos;
    // End of variables declaration//GEN-END:variables
}
