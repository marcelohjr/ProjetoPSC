package Apresentação;

import DataAccess.MercadoriaFornecedorDAO;
import DataAccess.NotaFiscalDAO;
import DataAccess.NotaMercadoriaDAO;
import DomainModel.FContexto;
import DomainModel.Fornecedor;
import DomainModel.Mercadoria;
import DomainModel.MercadoriaFornecedor;
import DomainModel.NotaFiscal;
import DomainModel.NotaMercadoria;
import static Validações.validaData.compareData;
import static Validações.validaData.isData;
import static Validações.validaData.isDataMenorHoje;
import static java.lang.Float.parseFloat;
import static java.lang.Long.parseLong;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class JFTelaNotaFiscal extends javax.swing.JFrame {

    public JFTelaNotaFiscal() {
        initComponents();
        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        jTProdutos.setRowSorter(new TableRowSorter(modelo));
        jTFValorTotal.getDocument().addDocumentListener(docListener);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTFCodigo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTFNumero = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTFCnpj = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        jTFCpf = new javax.swing.JFormattedTextField();
        jLabel8 = new javax.swing.JLabel();
        jTFNome = new javax.swing.JTextField();
        jBBuscarFornecedor = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTProdutos = new javax.swing.JTable();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jBNovo = new javax.swing.JButton();
        jBBuscar = new javax.swing.JButton();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBSalvar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jBAdicionar = new javax.swing.JButton();
        jBRemover = new javax.swing.JButton();
        jTFValorTotal = new javax.swing.JFormattedTextField();
        jTFAcrescimos = new javax.swing.JFormattedTextField();
        jTFDescontos = new javax.swing.JFormattedTextField();
        jTFCodigoFornecedor = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jDCDataEmissao = new com.toedter.calendar.JDateChooser();
        jDCDataEntrega = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tela de Notas Fiscais");

        jLabel1.setText("Código");

        jTFCodigo.setEnabled(false);

        jLabel2.setText("N° da Nota");

        jTFNumero.setEnabled(false);
        jTFNumero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFNumeroKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTFNumeroKeyTyped(evt);
            }
        });

        jLabel3.setText("Data Emissão");

        jLabel4.setText("Data Entrega");

        jSeparator1.setToolTipText("");

        jLabel5.setText("Fornecedor");

        jLabel6.setText("CNPJ");

        try {
            jTFCnpj.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###/####-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jTFCnpj.setEnabled(false);

        jLabel7.setText("CPF");

        try {
            jTFCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jTFCpf.setEnabled(false);

        jLabel8.setText("Nome");

        jTFNome.setEnabled(false);

        jBBuscarFornecedor.setText("Buscar");
        jBBuscarFornecedor.setEnabled(false);
        jBBuscarFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarFornecedorActionPerformed(evt);
            }
        });

        jLabel9.setText("Produtos");

        jTProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Descrição", "Preço Unitário", "Quantidade", "Preço Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Float.class, java.lang.Integer.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTProdutos.setEnabled(false);
        jTProdutos.getTableHeader().setReorderingAllowed(false);
        jTProdutos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTProdutosMouseClicked(evt);
            }
        });
        jTProdutos.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTProdutosPropertyChange(evt);
            }
        });
        jScrollPane2.setViewportView(jTProdutos);
        if (jTProdutos.getColumnModel().getColumnCount() > 0) {
            jTProdutos.getColumnModel().getColumn(0).setResizable(false);
            jTProdutos.getColumnModel().getColumn(0).setPreferredWidth(30);
            jTProdutos.getColumnModel().getColumn(1).setResizable(false);
            jTProdutos.getColumnModel().getColumn(1).setPreferredWidth(200);
            jTProdutos.getColumnModel().getColumn(2).setResizable(false);
            jTProdutos.getColumnModel().getColumn(2).setPreferredWidth(50);
            jTProdutos.getColumnModel().getColumn(3).setResizable(false);
            jTProdutos.getColumnModel().getColumn(3).setPreferredWidth(50);
            jTProdutos.getColumnModel().getColumn(4).setResizable(false);
            jTProdutos.getColumnModel().getColumn(4).setPreferredWidth(50);
        }

        jLabel10.setText("Descontos");

        jLabel11.setText("Acréscimos");

        jLabel12.setText("Valor Total");

        jBNovo.setText("Novo");
        jBNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBNovoActionPerformed(evt);
            }
        });

        jBBuscar.setText("Buscar");
        jBBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarActionPerformed(evt);
            }
        });

        jBAlterar.setText("Alterar");
        jBAlterar.setEnabled(false);
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });

        jBExcluir.setText("Excluir");
        jBExcluir.setEnabled(false);
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });

        jBSalvar.setText("Salvar");
        jBSalvar.setEnabled(false);
        jBSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSalvarActionPerformed(evt);
            }
        });

        jBCancelar.setText("Cancelar");
        jBCancelar.setEnabled(false);
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });

        jBAdicionar.setText("Adicionar");
        jBAdicionar.setEnabled(false);
        jBAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAdicionarActionPerformed(evt);
            }
        });

        jBRemover.setText("Remover");
        jBRemover.setEnabled(false);
        jBRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBRemoverActionPerformed(evt);
            }
        });

        jTFValorTotal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));
        jTFValorTotal.setEnabled(false);
        jTFValorTotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTFValorTotalKeyTyped(evt);
            }
        });

        jTFAcrescimos.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));
        jTFAcrescimos.setText("0,00");
        jTFAcrescimos.setEnabled(false);
        jTFAcrescimos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFAcrescimosKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTFAcrescimosKeyTyped(evt);
            }
        });

        jTFDescontos.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));
        jTFDescontos.setText("0,00");
        jTFDescontos.setEnabled(false);
        jTFDescontos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFDescontosKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTFDescontosKeyTyped(evt);
            }
        });

        jTFCodigoFornecedor.setEnabled(false);

        jLabel13.setText("Código");

        jDCDataEmissao.setEnabled(false);
        jDCDataEmissao.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jDCDataEmissaoPropertyChange(evt);
            }
        });

        jDCDataEntrega.setEnabled(false);
        jDCDataEntrega.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jDCDataEntregaPropertyChange(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1)
                            .addComponent(jSeparator2)
                            .addComponent(jScrollPane2)
                            .addComponent(jSeparator3)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel2)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTFNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jDCDataEmissao, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jDCDataEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel13))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTFNome, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jTFCodigoFornecedor)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTFCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel7)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTFCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jBBuscarFornecedor)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jBNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel10))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jBBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jBAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jBExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jBSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jBCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jTFDescontos, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel11)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTFAcrescimos, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel12)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTFValorTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(191, 191, 191)
                                .addComponent(jBAdicionar)
                                .addGap(123, 123, 123)
                                .addComponent(jBRemover))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(305, 305, 305)
                                .addComponent(jLabel9)))
                        .addGap(27, 27, 27)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(297, 297, 297)
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jTFNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel3))
                    .addComponent(jDCDataEmissao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDCDataEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTFCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jTFCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTFCodigoFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jTFNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBBuscarFornecedor))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBAdicionar)
                    .addComponent(jBRemover))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12)
                    .addComponent(jTFValorTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTFAcrescimos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTFDescontos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBNovo)
                    .addComponent(jBBuscar)
                    .addComponent(jBAlterar)
                    .addComponent(jBExcluir)
                    .addComponent(jBSalvar)
                    .addComponent(jBCancelar))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBBuscarFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarFornecedorActionPerformed
        FContexto.getListarFornecedores().setVisible(true);
        FContexto.getListarFornecedores().readJTable();
    }//GEN-LAST:event_jBBuscarFornecedorActionPerformed

    private void jTFNumeroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFNumeroKeyTyped
        String caracteres = "0987654321";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_jTFNumeroKeyTyped

    private void jTFValorTotalKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFValorTotalKeyTyped
        String caracteres = "0987654321,";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_jTFValorTotalKeyTyped

    private void jTFAcrescimosKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFAcrescimosKeyTyped
        String caracteres = "0987654321,";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_jTFAcrescimosKeyTyped

    private void jTFDescontosKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFDescontosKeyTyped
        String caracteres = "0987654321,";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_jTFDescontosKeyTyped

    private void jBAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAdicionarActionPerformed
        FContexto.getListarMercadorias().setVisible(true);
        FContexto.getListarMercadorias().readJTable();
    }//GEN-LAST:event_jBAdicionarActionPerformed

    private void jBRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBRemoverActionPerformed
        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        if (jTProdutos.getSelectedRow() >= 0) {
            modelo.removeRow(jTProdutos.getSelectedRow());
            jTProdutos.setModel(modelo);
        }

        int size = modelo.getRowCount();
        float valorTotal = (float) 0.0;

        for (int i = 0; i < size; i++) {
            valorTotal += (float) jTProdutos.getModel().getValueAt(i, 4);
        }

        String valor = String.valueOf(valorTotal);
        valor = valor.replace(".", ",");
        jTFValorTotal.setText(String.valueOf(valor));
        jTFDescontos.setText("0,00");
        jTFAcrescimos.setText("0,00");

        if (verificaCampos()) {
            jBSalvar.setEnabled(true);
        } else {
            jBSalvar.setEnabled(false);
        }
    }//GEN-LAST:event_jBRemoverActionPerformed

    private void jTProdutosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTProdutosMouseClicked
        if (jTProdutos.getSelectedRow() != -1) {
            jBRemover.setEnabled(true);
        }
    }//GEN-LAST:event_jTProdutosMouseClicked

    private void jTProdutosPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTProdutosPropertyChange
        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        int size = modelo.getRowCount();
        int qt;
        float vu, valorTotal = (float) 0.0;

        for (int i = 0; i < size; i++) {
            qt = (int) jTProdutos.getModel().getValueAt(i, 3);
            vu = (float) jTProdutos.getModel().getValueAt(i, 2);

            jTProdutos.getModel().setValueAt((qt * vu), i, 4);
        }

        for (int i = 0; i < size; i++) {
            valorTotal += (float) jTProdutos.getModel().getValueAt(i, 4);
        }

        String valor = String.valueOf(valorTotal);
        valor = valor.replace(".", ",");
        jTFValorTotal.setText(String.valueOf(valor));
    }//GEN-LAST:event_jTProdutosPropertyChange

    private void jTFDescontosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFDescontosKeyReleased
        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        int size = modelo.getRowCount();
        float vTotal = (float) 0.0;
        float desc, acre;

        for (int i = 0; i < size; i++) {
            vTotal += (float) jTProdutos.getModel().getValueAt(i, 4);
        }

        String acrescimo = jTFAcrescimos.getText();
        acrescimo = acrescimo.replace(",", ".");

        if (acrescimo.equals("")) {
            acre = (float) 0.0;
        } else {
            try{
                acre = parseFloat(acrescimo);
            }catch(Exception e){
                acre = (float) 0.0;
            }
        }

        vTotal += acre;

        String desconto = jTFDescontos.getText();
        desconto = desconto.replace(",", ".");

        if (desconto.equals("")) {
            desc = (float) 0.0;
        } else {
            try{
                desc = parseFloat(desconto);
            }catch(Exception e){
                desc = (float) 0.0;
            }
        }

        vTotal -= desc;

        String valorTotal = String.valueOf(vTotal);
        valorTotal = valorTotal.replace(".", ",");
        jTFValorTotal.setText(valorTotal);
    }//GEN-LAST:event_jTFDescontosKeyReleased

    private void jTFAcrescimosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFAcrescimosKeyReleased
        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        int size = modelo.getRowCount();
        float vTotal = (float) 0.0;
        float acre, desc;

        for (int i = 0; i < size; i++) {
            vTotal += (float) jTProdutos.getModel().getValueAt(i, 4);
        }

        String desconto = jTFDescontos.getText();
        desconto = desconto.replace(",", ".");

        if (desconto.equals("")) {
            desc = (float) 0.0;
        } else {
            try{
                desc = parseFloat(desconto);
            }catch(Exception e){
                desc = (float) 0.0;
            }
        }

        vTotal -= desc;

        String acrescimo = jTFAcrescimos.getText();
        acrescimo = acrescimo.replace(",", ".");

        if (acrescimo.equals("")) {
            acre = (float) 0.0;
        } else {
            try{
                acre = parseFloat(acrescimo);
            }catch(Exception e){
                acre = (float) 0.0;
            }
        }

        vTotal += acre;

        String valorTotal = String.valueOf(vTotal);
        valorTotal = valorTotal.replace(".", ",");
        jTFValorTotal.setText(valorTotal);
    }//GEN-LAST:event_jTFAcrescimosKeyReleased

    private void jBSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSalvarActionPerformed
        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        int size = modelo.getRowCount();
        NotaFiscal notaFiscal = new NotaFiscal();
        NotaMercadoria notaMercadoria;
        MercadoriaFornecedor mercadoriaFornecedor;
        NotaFiscalDAO ndao = new NotaFiscalDAO();
        NotaMercadoriaDAO nmdao = new NotaMercadoriaDAO();
        MercadoriaFornecedorDAO mfdao = new MercadoriaFornecedorDAO();
        boolean valido = true;

        String descontos = jTFDescontos.getText();
        String acrescimos = jTFAcrescimos.getText();
        String valorTotal = jTFValorTotal.getText();
        descontos = descontos.replace(".", "");
        descontos = descontos.replace(",", ".");
        acrescimos = acrescimos.replace(".", "");
        acrescimos = acrescimos.replace(",", ".");
        valorTotal = valorTotal.replace(".", "");
        valorTotal = valorTotal.replace(",", ".");

        for (NotaFiscal n : ndao.findAll()) {
            if (n.getNumero() == parseLong(jTFNumero.getText())) {
                if (jTFCodigo.getText().equals("")) {
                    valido = false;
                    break;
                }
            }
        }

        if (!valido) {
            JOptionPane.showMessageDialog(this, " Já existe uma Nota Fiscal com esse número!");
        }

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String dataEmissao = formato.format(jDCDataEmissao.getDate());
        String dataEntrega = formato.format(jDCDataEntrega.getDate());

        if (!isData(dataEmissao) || !isDataMenorHoje(dataEmissao)) {
            valido = false;
            JOptionPane.showMessageDialog(this, " Data de Emissão Inválida!");
            jDCDataEmissao.setDate(null);
        }

        if (!isData(dataEntrega) || !isDataMenorHoje(dataEntrega)) {
            valido = false;
            JOptionPane.showMessageDialog(this, " Data de Entrega Inválida!");
            jDCDataEntrega.setDate(null);
        }

        if (valido) {
            try {
                if (compareData(dataEmissao, dataEntrega) > 0) {
                    valido = false;
                    JOptionPane.showMessageDialog(this, " A Data de Emissão não pode ser maior que a Data de Entrega!");
                }
            } catch (ParseException ex) {
                Logger.getLogger(JFTelaNotaFiscal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (valido) {
            notaFiscal.setNumero(Long.parseLong(jTFNumero.getText()));

            notaFiscal.setDataEmissao(jDCDataEmissao.getDate());
            notaFiscal.setDataEntrega(jDCDataEntrega.getDate());
            notaFiscal.setIdFornecedor(Integer.parseInt(jTFCodigoFornecedor.getText()));

            if (jTFCnpj.getText().equals("  .   .   /    -  ")) {
                notaFiscal.setCpf(jTFCpf.getText());
            } else {
                notaFiscal.setCnpj(jTFCnpj.getText());
            }

            notaFiscal.setDescricaoFornecedor(jTFNome.getText());

            if (descontos.equals("")) {
                notaFiscal.setDescontos((float) 0.0);
            } else {
                notaFiscal.setDescontos(Float.parseFloat(descontos));
            }

            if (acrescimos.equals("")) {
                notaFiscal.setAcrescimos((float) 0.0);
            } else {
                notaFiscal.setAcrescimos(Float.parseFloat(acrescimos));
            }

            notaFiscal.setValorTotal(Float.parseFloat(valorTotal));

            boolean logica = true;

            if (jTFCodigo.getText().equals("")) {
                NotaFiscalDAO.getInstance().persist(notaFiscal);

                for (int i = 0; i < size; i++) {

                    mercadoriaFornecedor = new MercadoriaFornecedor();

                    for (MercadoriaFornecedor mf : mfdao.findAll()) {
                        if (mf.getIdFornecedor() == notaFiscal.getIdFornecedor() && mf.getIdMercadoria() == (int) jTProdutos.getModel().getValueAt(i, 0)) {
                            logica = false;
                            break;
                        }
                    }

                    if (logica) {
                        mercadoriaFornecedor.setIdFornecedor(Integer.parseInt(jTFCodigoFornecedor.getText()));
                        mercadoriaFornecedor.setIdMercadoria((int) jTProdutos.getModel().getValueAt(i, 0));
                        MercadoriaFornecedorDAO.getInstance().persist(mercadoriaFornecedor);
                    }

                }

                for (int i = 0; i < size; i++) {

                    notaMercadoria = new NotaMercadoria();

                    for (NotaFiscal n : ndao.findAll()) {
                        if (n.getNumero() == notaFiscal.getNumero()) {
                            notaMercadoria.setIdNota(n.getCodigo());
                            break;
                        }
                    }

                    notaMercadoria.setIdMercadoria((int) jTProdutos.getModel().getValueAt(i, 0));
                    notaMercadoria.setDescricao((String) jTProdutos.getModel().getValueAt(i, 1));
                    notaMercadoria.setPrecoUnitario((float) jTProdutos.getModel().getValueAt(i, 2));
                    notaMercadoria.setQuantidade((int) jTProdutos.getModel().getValueAt(i, 3));
                    NotaMercadoriaDAO.getInstance().persist(notaMercadoria);
                }

            } else {
                logica = true;
                notaFiscal.setCodigo(Integer.parseInt(jTFCodigo.getText()));
                NotaFiscalDAO.getInstance().merge(notaFiscal);

                for (int i = 0; i < size; i++) {

                    mercadoriaFornecedor = new MercadoriaFornecedor();

                    for (MercadoriaFornecedor mf : mfdao.findAll()) {
                        if (mf.getIdFornecedor() == notaFiscal.getIdFornecedor() && mf.getIdMercadoria() == (int) jTProdutos.getModel().getValueAt(i, 0)) {
                            logica = false;
                            break;
                        }
                    }

                    if (logica) {
                        mercadoriaFornecedor.setIdFornecedor(Integer.parseInt(jTFCodigoFornecedor.getText()));
                        mercadoriaFornecedor.setIdMercadoria((int) jTProdutos.getModel().getValueAt(i, 0));
                        MercadoriaFornecedorDAO.getInstance().persist(mercadoriaFornecedor);
                    }

                }

                logica = true;

                for (int i = 0; i < size; i++) {

                    notaMercadoria = new NotaMercadoria();

                    for (NotaMercadoria nm : nmdao.findAll()) {
                        if (nm.getIdNota() == notaFiscal.getCodigo() && nm.getIdMercadoria() == (int) jTProdutos.getModel().getValueAt(i, 0)) {
                            logica = false;
                            break;
                        }
                    }

                    if (logica) {
                        notaMercadoria.setIdNota(Integer.parseInt(jTFCodigo.getText()));
                        notaMercadoria.setIdMercadoria((int) jTProdutos.getModel().getValueAt(i, 0));
                        notaMercadoria.setDescricao((String) jTProdutos.getModel().getValueAt(i, 1));
                        notaMercadoria.setPrecoUnitario((float) jTProdutos.getModel().getValueAt(i, 2));
                        notaMercadoria.setQuantidade((int) jTProdutos.getModel().getValueAt(i, 3));
                    }
                }
            }

            JOptionPane.showMessageDialog(this, " Salvo com sucesso!");
            inicio();
        }
    }//GEN-LAST:event_jBSalvarActionPerformed

    private void jBNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBNovoActionPerformed
        limpar();
        habilitarCampos();
        jBCancelar.setEnabled(true);
        jBNovo.setEnabled(false);
    }//GEN-LAST:event_jBNovoActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        inicio();
    }//GEN-LAST:event_jBCancelarActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
        habilitarCampos();
        jBAlterar.setEnabled(false);
        jBBuscar.setEnabled(false);
        jBSalvar.setEnabled(true);
        jBNovo.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBCancelar.setEnabled(true);
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        NotaMercadoriaDAO nmdao = new NotaMercadoriaDAO();
        int codigo = Integer.parseInt(jTFCodigo.getText());
        NotaFiscalDAO.getInstance().removeById(codigo);

        for (NotaMercadoria nm : nmdao.findAll()) {
            if (codigo == nm.getIdNota()) {
                nmdao.remove(nm);
            }
        }

        JOptionPane.showMessageDialog(this, " Nota Fiscal excluída com sucesso!");
        inicio();
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jTFNumeroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFNumeroKeyReleased
        if (verificaCampos()) {
            jBSalvar.setEnabled(true);
        } else {
            jBSalvar.setEnabled(false);
        }
    }//GEN-LAST:event_jTFNumeroKeyReleased

    private void jBBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarActionPerformed
        FContexto.getListarNotasFiscais().setVisible(true);
        FContexto.getListarNotasFiscais().readJTable();
    }//GEN-LAST:event_jBBuscarActionPerformed

    private void jDCDataEmissaoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jDCDataEmissaoPropertyChange
        if (verificaCampos()) {
            jBSalvar.setEnabled(true);
        } else {
            jBSalvar.setEnabled(false);
        }
    }//GEN-LAST:event_jDCDataEmissaoPropertyChange

    private void jDCDataEntregaPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jDCDataEntregaPropertyChange
        if (verificaCampos()) {
            jBSalvar.setEnabled(true);
        } else {
            jBSalvar.setEnabled(false);
        }
    }//GEN-LAST:event_jDCDataEntregaPropertyChange

    public void setNotaFiscal(NotaFiscal notaFiscal) {
        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        modelo.setNumRows(0);
        NotaMercadoriaDAO nmdao = new NotaMercadoriaDAO();

        jTFCodigo.setText(String.valueOf(notaFiscal.getCodigo()));
        jTFNumero.setText(String.valueOf(notaFiscal.getNumero()));

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String dataEmissao = formato.format(notaFiscal.getDataEmissao());
        String dataEntrega = formato.format(notaFiscal.getDataEntrega());

        try {
            jDCDataEmissao.setDate(formato.parse(dataEmissao));
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaNotaFiscal.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            jDCDataEntrega.setDate(formato.parse(dataEntrega));
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaNotaFiscal.class.getName()).log(Level.SEVERE, null, ex);
        }

        jTFCodigoFornecedor.setText(String.valueOf(notaFiscal.getIdFornecedor()));
        jTFCnpj.setText(notaFiscal.getCnpj());
        jTFCpf.setText(notaFiscal.getCpf());
        jTFNome.setText(notaFiscal.getDescricaoFornecedor());

        String descontos = String.valueOf(notaFiscal.getDescontos());
        String acrescimos = String.valueOf(notaFiscal.getAcrescimos());
        String valorTotal = String.valueOf(notaFiscal.getValorTotal());
        descontos = descontos.replace(".", ",");
        acrescimos = acrescimos.replace(".", ",");
        valorTotal = valorTotal.replace(".", ",");

        jTFDescontos.setText(descontos);
        jTFAcrescimos.setText(acrescimos);
        jTFValorTotal.setText(valorTotal);

        for (NotaMercadoria nm : nmdao.findAll()) {
            if (notaFiscal.getCodigo() == nm.getIdNota()) {
                modelo.addRow(new Object[]{
                    nm.getIdMercadoria(),
                    nm.getDescricao(),
                    nm.getPrecoUnitario(),
                    nm.getQuantidade(),
                    nm.getPrecoUnitario() * nm.getQuantidade()
                });
            }
        }

        jBExcluir.setEnabled(true);
        jBAlterar.setEnabled(true);
        jBCancelar.setEnabled(true);
        jBSalvar.setEnabled(false);
    }

    public void setFornecedor(Fornecedor fornecedor) {
        jTFCodigoFornecedor.setText(String.valueOf(fornecedor.getCodigo()));
        jTFNome.setText(fornecedor.getNome());

        if (fornecedor.getTipo() == 1) {
            jTFCnpj.setText(fornecedor.getCnpj());
        } else {
            jTFCpf.setText(fornecedor.getCpf());
        }

        if (verificaCampos()) {
            jBSalvar.setEnabled(true);
        } else {
            jBSalvar.setEnabled(false);
        }
    }

    public void setMercadoria(Mercadoria m) {
        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        int size = modelo.getRowCount();
        boolean valido = true;
        int codigo;
        float valorTotal = (float) 0.0;

        for (int i = 0; i < size; i++) {
            codigo = (int) jTProdutos.getModel().getValueAt(i, 0);
            if (m.getCodigo() == codigo) {
                valido = false;
            }
        }

        if (valido) {
            modelo.addRow(new Object[]{
                m.getCodigo(),
                m.getNome(),
                m.getPrecoCompra(),
                1,
                m.getPrecoCompra()
            });
        }

        size = modelo.getRowCount();

        for (int i = 0; i < size; i++) {
            valorTotal += (float) jTProdutos.getModel().getValueAt(i, 4);
        }

        String valor = String.valueOf(valorTotal);
        valor = valor.replace(".", ",");
        jTFValorTotal.setText(valor);

        if (verificaCampos()) {
            jBSalvar.setEnabled(true);
        } else {
            jBSalvar.setEnabled(false);
        }
    }

    DocumentListener docListener = new DocumentListener() {

        @Override
        public void insertUpdate(DocumentEvent evt) {
            atualizaTotal();
        }

        @Override
        public void removeUpdate(DocumentEvent evt) {
        }

        @Override
        public void changedUpdate(DocumentEvent evt) {
        }

        public void atualizaTotal() {
            DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
            String vt = jTFValorTotal.getText();
            vt = vt.replace(",", ".");
            int size;
            float valorTotal;

            if (vt.equals("")) {
                valorTotal = (float) 0.0;
            } else {
                valorTotal = parseFloat(vt);
            }

            if (valorTotal < 0) {
                JOptionPane.showMessageDialog(rootPane, " Valor Total Inválido! Tente de novo!");
                jTFDescontos.setText("0,00");
                jTFAcrescimos.setText("0,00");

                size = modelo.getRowCount();
                valorTotal = (float) 0.0;

                for (int i = 0; i < size; i++) {
                    valorTotal += (float) jTProdutos.getModel().getValueAt(i, 4);
                }

                vt = String.valueOf(valorTotal);
                vt = vt.replace(".", ",");
                load(vt);
            }
        }
    };

    private void inicio() {
        limpar();
        desabilitarCampos();
        jBNovo.setEnabled(true);
        jBBuscar.setEnabled(true);
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBSalvar.setEnabled(false);
        jBCancelar.setEnabled(false);
    }

    void desabilitarCampos() {
        jTFNumero.setEnabled(false);
        jDCDataEmissao.setEnabled(false);
        jDCDataEntrega.setEnabled(false);
        jTFDescontos.setEnabled(false);
        jTFAcrescimos.setEnabled(false);
        jTProdutos.setEnabled(false);
        jBBuscarFornecedor.setEnabled(false);
        jBAdicionar.setEnabled(false);
    }

    private void habilitarCampos() {
        jTFNumero.setEnabled(true);
        jDCDataEmissao.setEnabled(true);
        jDCDataEntrega.setEnabled(true);
        jTFDescontos.setEnabled(true);
        jTFAcrescimos.setEnabled(true);
        jTProdutos.setEnabled(true);
        jBBuscarFornecedor.setEnabled(true);
        jBAdicionar.setEnabled(true);
    }

    private void limpar() {
        jTFCodigo.setText("");
        jTFCodigoFornecedor.setText("");
        jTFNome.setText("");
        jTFNumero.setText("");
        jDCDataEmissao.setDate(null);
        jDCDataEntrega.setDate(null);
        jTFCnpj.setText("");
        jTFCpf.setText("");
        jTFDescontos.setText("0,00");
        jTFAcrescimos.setText("0,00");
        jTFValorTotal.setText("");

        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        modelo.setNumRows(0);
    }

    public boolean verificaCampos() {
        boolean resp = true;

        if (jTFNumero.getText().equals("")) {
            resp = false;
        }

        if (jTFCodigoFornecedor.getText().equals("")) {
            resp = false;
        }

        if (jDCDataEmissao.getDate() == null) {
            resp = false;
        }

        if (jDCDataEntrega.getDate() == null) {
            resp = false;
        }

        DefaultTableModel modelo = (DefaultTableModel) jTProdutos.getModel();
        int size = modelo.getRowCount();

        if (size < 1) {
            resp = false;
        }

        if (jTFValorTotal.getText().equals("")) {
            resp = false;
        }

        return resp;
    }

    public void load(final String text) {
        javax.swing.SwingUtilities.invokeLater(() -> {
            jTFValorTotal.setText(text);
        });
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFTelaNotaFiscal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFTelaNotaFiscal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFTelaNotaFiscal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFTelaNotaFiscal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFTelaNotaFiscal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBAdicionar;
    private javax.swing.JButton jBAlterar;
    private javax.swing.JButton jBBuscar;
    private javax.swing.JButton jBBuscarFornecedor;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBNovo;
    private javax.swing.JButton jBRemover;
    private javax.swing.JButton jBSalvar;
    private com.toedter.calendar.JDateChooser jDCDataEmissao;
    private com.toedter.calendar.JDateChooser jDCDataEntrega;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JFormattedTextField jTFAcrescimos;
    private javax.swing.JFormattedTextField jTFCnpj;
    private javax.swing.JTextField jTFCodigo;
    private javax.swing.JTextField jTFCodigoFornecedor;
    private javax.swing.JFormattedTextField jTFCpf;
    private javax.swing.JFormattedTextField jTFDescontos;
    private javax.swing.JTextField jTFNome;
    private javax.swing.JTextField jTFNumero;
    private javax.swing.JFormattedTextField jTFValorTotal;
    private javax.swing.JTable jTProdutos;
    // End of variables declaration//GEN-END:variables
}
