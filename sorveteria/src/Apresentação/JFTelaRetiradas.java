package Apresentação;

import DataAccess.CaixaDAO;
import DataAccess.RetiradasDAO;
import DomainModel.Caixa;
import DomainModel.Retiradas;
import static Validações.validaData.compareData;
import static Validações.validaData.isData;
import static Validações.validaData.isDataMenorHoje;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class JFTelaRetiradas extends javax.swing.JFrame {

    public JFTelaRetiradas() {
        initComponents();
        jTFCodigo.setVisible(false);
        jDCDataInicio.setDate(new Date());
        jDCDataFim.setDate(new Date());
        ((JTextField) jDCDataInicio.getDateEditor()).setEditable(false);
        ((JTextField) jDCDataFim.getDateEditor()).setEditable(false);
        DefaultTableModel modelo = (DefaultTableModel) jTRetiradas.getModel();
        jTRetiradas.setRowSorter(new TableRowSorter(modelo));
        jTRetiradas.getColumnModel().getColumn(0).setMaxWidth(0);
        jTRetiradas.getColumnModel().getColumn(0).setMinWidth(0);
        jTRetiradas.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
        jTRetiradas.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
        readJTableForDate();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jTFDescrição = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTFData = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        jTFValor = new javax.swing.JFormattedTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jBSalvar = new javax.swing.JButton();
        jBNovo = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTRetiradas = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jBFiltrar = new javax.swing.JButton();
        jBListar = new javax.swing.JButton();
        jBFechar = new javax.swing.JButton();
        jTFCodigo = new javax.swing.JTextField();
        jDCDataInicio = new com.toedter.calendar.JDateChooser();
        jDCDataFim = new com.toedter.calendar.JDateChooser();
        jBExcluir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tela de Retiradas");

        jLabel2.setText("Descrição");

        jTFDescrição.setEnabled(false);
        jTFDescrição.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFDescriçãoKeyReleased(evt);
            }
        });

        jLabel3.setText("Data");

        jTFData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));
        jTFData.setEnabled(false);

        jLabel4.setText("Valor");

        jTFValor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));
        jTFValor.setEnabled(false);
        jTFValor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFValorKeyReleased(evt);
            }
        });

        jBSalvar.setText("Salvar");
        jBSalvar.setEnabled(false);
        jBSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSalvarActionPerformed(evt);
            }
        });

        jBNovo.setText("Novo");
        jBNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBNovoActionPerformed(evt);
            }
        });

        jTRetiradas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Data", "Descrição", "Valor"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTRetiradas.getTableHeader().setReorderingAllowed(false);
        jTRetiradas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTRetiradasMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jTRetiradas);
        if (jTRetiradas.getColumnModel().getColumnCount() > 0) {
            jTRetiradas.getColumnModel().getColumn(0).setResizable(false);
            jTRetiradas.getColumnModel().getColumn(0).setPreferredWidth(0);
            jTRetiradas.getColumnModel().getColumn(1).setResizable(false);
            jTRetiradas.getColumnModel().getColumn(2).setResizable(false);
            jTRetiradas.getColumnModel().getColumn(2).setPreferredWidth(350);
            jTRetiradas.getColumnModel().getColumn(3).setResizable(false);
        }

        jLabel1.setText("Data Início");

        jLabel5.setText("Data Fim");

        jBFiltrar.setText("Filtrar");
        jBFiltrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBFiltrarActionPerformed(evt);
            }
        });

        jBListar.setText("Listar Todos");
        jBListar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBListarActionPerformed(evt);
            }
        });

        jBFechar.setText("Fechar");
        jBFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBFecharActionPerformed(evt);
            }
        });

        jTFCodigo.setEnabled(false);

        jBExcluir.setText("Excluir");
        jBExcluir.setEnabled(false);
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTFDescrição, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTFValor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jBNovo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTFData, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(293, 293, 293)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jBExcluir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jBSalvar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jDCDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jDCDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jBFiltrar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jBListar)
                        .addGap(18, 18, 18)
                        .addComponent(jBFechar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jSeparator1))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jTFData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jBExcluir)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTFDescrição, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jTFValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBSalvar)
                    .addComponent(jBNovo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jBFiltrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jBListar)
                            .addComponent(jBFechar)))
                    .addComponent(jDCDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDCDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBListarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBListarActionPerformed
        readJTable();
    }//GEN-LAST:event_jBListarActionPerformed

    private void jTFDescriçãoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFDescriçãoKeyReleased
        if (verificaCampos()) {
            jBSalvar.setEnabled(true);
        } else {
            jBSalvar.setEnabled(false);
        }
    }//GEN-LAST:event_jTFDescriçãoKeyReleased

    private void jTFValorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFValorKeyReleased
        if (verificaCampos()) {
            jBSalvar.setEnabled(true);
        } else {
            jBSalvar.setEnabled(false);
        }
    }//GEN-LAST:event_jTFValorKeyReleased

    private void jBNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBNovoActionPerformed
        String data;
        limpar();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        data = formatter.format(new Date());
        jTFData.setText(data);
        habilitarCampos();
        jBSalvar.setEnabled(false);
    }//GEN-LAST:event_jBNovoActionPerformed

    private void jBSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSalvarActionPerformed
        Retiradas retiradas = new Retiradas();
        boolean valido = true;

        if (jTFValor.getText().equals("")) {
            valido = false;
            JOptionPane.showMessageDialog(this, " Valor Inválido!");
        }

        if (valido) {
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            Date data = null;

            try {
                data = formato.parse(jTFData.getText());
            } catch (ParseException ex) {
                Logger.getLogger(JFTelaRetiradas.class.getName()).log(Level.SEVERE, null, ex);
            }

            retiradas.setData(data);
            retiradas.setDescricao(jTFDescrição.getText());

            String valor = jTFValor.getText();
            valor = valor.replace(".", "");
            valor = valor.replace(",", ".");

            retiradas.setValor(Float.parseFloat(valor));

            if (jTFCodigo.getText().equals("")) {
                RetiradasDAO.getInstance().persist(retiradas);
            } else {
                retiradas.setCodigo(Integer.parseInt(jTFCodigo.getText()));
                RetiradasDAO.getInstance().merge(retiradas);
            }

            JOptionPane.showMessageDialog(this, " Salvo com sucesso!");
            inicio();
        }
    }//GEN-LAST:event_jBSalvarActionPerformed

    private void jBFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBFecharActionPerformed
        dispose();
    }//GEN-LAST:event_jBFecharActionPerformed

    private void jBFiltrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBFiltrarActionPerformed
        boolean valido = true;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dataInicio = formatter.format(jDCDataInicio.getDate());
        String dataFim = formatter.format(jDCDataFim.getDate());

        if (!isData(dataInicio) || !isDataMenorHoje(dataInicio)) {
            valido = false;
            JOptionPane.showMessageDialog(this, " Data de Início Inválida!");
        }

        if (!isData(dataFim) || !isDataMenorHoje(dataFim)) {
            valido = false;
            JOptionPane.showMessageDialog(this, " Data Final Inválida!");
        }

        if (valido) {
            try {
                if (compareData(dataInicio, dataFim) > 0) {
                    valido = false;
                    JOptionPane.showMessageDialog(this, " A Data de Início não pode ser maior que a Data Final!");
                }
            } catch (ParseException ex) {
                Logger.getLogger(JFTelaNotaFiscal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (valido) {
            readJTableForDate();
        }
    }//GEN-LAST:event_jBFiltrarActionPerformed

    private void jTRetiradasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTRetiradasMouseClicked
        jTFCodigo.setText(String.valueOf(jTRetiradas.getValueAt(jTRetiradas.getSelectedRow(), 0)));
        jTFData.setText((String) jTRetiradas.getValueAt(jTRetiradas.getSelectedRow(), 1));
        jTFDescrição.setText((String) jTRetiradas.getValueAt(jTRetiradas.getSelectedRow(), 2));
        jTFValor.setText(String.valueOf(jTRetiradas.getValueAt(jTRetiradas.getSelectedRow(), 3)));

        boolean valido = false;
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String data = formato.format(new Date());
        Date dataHoje = null;

        try {
            dataHoje = formato.parse(data);
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaRetiradas.class.getName()).log(Level.SEVERE, null, ex);
        }

        Date dataRetirada = null;

        try {
            dataRetirada = formato.parse(jTFData.getText());
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaRetiradas.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (dataHoje.compareTo(dataRetirada) == 0) {
            valido = true;
        }

        if (valido && verificaCaixa()) {
            jBSalvar.setEnabled(true);
            jBExcluir.setEnabled(true);
            habilitarCampos();
        } else if (!valido && verificaCaixa()) {
            JOptionPane.showMessageDialog(this, " Somente retiradas de hoje podem ser alteradas!");
            jBSalvar.setEnabled(false);
            jBExcluir.setEnabled(false);
        } else if (!verificaCaixa()) {
            jBSalvar.setEnabled(false);
            jBExcluir.setEnabled(false);
        }
    }//GEN-LAST:event_jTRetiradasMouseClicked

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        long codigo = Long.parseLong(jTFCodigo.getText());
        RetiradasDAO.getInstance().removeById(codigo);

        JOptionPane.showMessageDialog(this, " Retirada excluída com sucesso!");
        inicio();
    }//GEN-LAST:event_jBExcluirActionPerformed

    public void readJTable() {

        DefaultTableModel modelo = (DefaultTableModel) jTRetiradas.getModel();
        modelo.setNumRows(0);
        RetiradasDAO rdao = new RetiradasDAO();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String data;

        for (Retiradas r : rdao.findAll()) {

            data = formatter.format(r.getData());

            modelo.addRow(new Object[]{
                r.getCodigo(),
                data,
                r.getDescricao(),
                r.getValor()
            });
        }
    }

    public void readJTableForDate() {

        DefaultTableModel modelo = (DefaultTableModel) jTRetiradas.getModel();
        modelo.setNumRows(0);
        RetiradasDAO rdao = new RetiradasDAO();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dataInicio = formatter.format(jDCDataInicio.getDate());
        String dataFim = formatter.format(jDCDataFim.getDate());
        String data;

        for (Retiradas r : rdao.findAll()) {

            data = formatter.format(r.getData());

            try {
                if (compareData(data, dataInicio) >= 0 && compareData(data, dataFim) <= 0) {
                    modelo.addRow(new Object[]{
                        r.getCodigo(),
                        data,
                        r.getDescricao(),
                        r.getValor()
                    });
                }
            } catch (ParseException ex) {
                Logger.getLogger(JFTelaRetiradas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean verificaCampos() {
        boolean resp = true;

        if (jTFDescrição.getText().equals("")) {
            resp = false;
        }
        if (jTFValor.getText().equals("")) {
            resp = false;
        }

        return resp;
    }

    public void limpar() {
        jTFDescrição.setText("");
        jTFValor.setText("");
        jTFData.setText("");
        jTFCodigo.setText("");
        readJTable();
    }

    private void inicio() {
        limpar();
        desabilitarCampos();
        jBSalvar.setEnabled(false);
        jBExcluir.setEnabled(false);
    }

    void desabilitarCampos() {
        jTFDescrição.setEnabled(false);
        jTFValor.setEnabled(false);
    }

    private void habilitarCampos() {
        jTFDescrição.setEnabled(true);
        jTFValor.setEnabled(true);
    }

    void caixaInvalido() {
        jTFDescrição.setEnabled(false);
        jTFValor.setEnabled(false);
        jBNovo.setEnabled(false);
        jBSalvar.setEnabled(false);
    }

    private boolean verificaCaixa() {
        boolean valido = false;
        CaixaDAO cdao = new CaixaDAO();
        Caixa caixa = new Caixa();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        String data = formato.format(new Date());
        Date dataHoje = null;

        try {
            dataHoje = formato.parse(data);
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaMercadorias.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Caixa cx : cdao.findAll()) {
            if (dataHoje.compareTo(cx.getData()) == 0) {
                valido = true;
                caixa = cx;
                break;
            }
        }

        if (!valido) {
            return false;
        }

        if (valido && caixa.isFechado()) {
            return false;
        }
        
        jBNovo.setEnabled(true);
        return valido;
    }

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFTelaRetiradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFTelaRetiradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFTelaRetiradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFTelaRetiradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFTelaRetiradas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBFechar;
    private javax.swing.JButton jBFiltrar;
    private javax.swing.JButton jBListar;
    private javax.swing.JButton jBNovo;
    private javax.swing.JButton jBSalvar;
    private com.toedter.calendar.JDateChooser jDCDataFim;
    private com.toedter.calendar.JDateChooser jDCDataInicio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jTFCodigo;
    private javax.swing.JFormattedTextField jTFData;
    private javax.swing.JTextField jTFDescrição;
    private javax.swing.JFormattedTextField jTFValor;
    private javax.swing.JTable jTRetiradas;
    // End of variables declaration//GEN-END:variables
}
