package Apresentação;

import DataAccess.CaixaDAO;
import DataAccess.RetiradasDAO;
import DomainModel.Caixa;
import DomainModel.Retiradas;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class JFTelaCaixaFechar extends javax.swing.JFrame {

    public JFTelaCaixaFechar() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jBFechar = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jTFSaldoFinal = new javax.swing.JTextField();
        jTFSaldoInicial = new javax.swing.JTextField();
        jTFTotalVendas = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTFRetiradas = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jBFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/finalizar.png"))); // NOI18N
        jBFechar.setText("Fechar Caixa");
        jBFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBFecharActionPerformed(evt);
            }
        });

        jTFSaldoFinal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTFSaldoFinal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTFSaldoFinal.setEnabled(false);

        jTFSaldoInicial.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTFSaldoInicial.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTFSaldoInicial.setEnabled(false);

        jTFTotalVendas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTFTotalVendas.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTFTotalVendas.setEnabled(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("Total Vendas");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Saldo Inicial");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Saldo Final");

        jTFRetiradas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTFRetiradas.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTFRetiradas.setEnabled(false);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Retiradas");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.DEFAULT_SIZE, 457, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(180, 180, 180)
                                .addComponent(jBFechar))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(119, 119, 119)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel1))
                                    .addComponent(jLabel4))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTFSaldoInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTFTotalVendas, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTFSaldoFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTFRetiradas, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFSaldoInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFTotalVendas, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFRetiradas, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFSaldoFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBFecharActionPerformed
        Caixa caixa = new Caixa();
        CaixaDAO cdao = new CaixaDAO();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        String data = formato.format(new Date());
        Date dataHoje = null;

        try {
            dataHoje = formato.parse(data);
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaMercadorias.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Caixa cx : cdao.findAll()) {
            if (dataHoje.compareTo(cx.getData()) == 0) {
                caixa = cx;
                break;
            }
        }

        caixa.setFechado(true);
        CaixaDAO.getInstance().merge(caixa);
        JOptionPane.showMessageDialog(this, " Caixa fechado com sucesso!");
        dispose();
    }//GEN-LAST:event_jBFecharActionPerformed

    void atualizaCaixa() {
        Caixa caixa = new Caixa();
        CaixaDAO cdao = new CaixaDAO();
        RetiradasDAO rdao = new RetiradasDAO();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        String data = formato.format(new Date());
        Date dataHoje = null;

        try {
            dataHoje = formato.parse(data);
        } catch (ParseException ex) {
            Logger.getLogger(JFTelaMercadorias.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Caixa cx : cdao.findAll()) {
            if (dataHoje.compareTo(cx.getData()) == 0) {
                caixa = cx;
                break;
            }
        }

        jTFSaldoInicial.setText(String.valueOf(caixa.getSaldoInicial()));
        jTFTotalVendas.setText(String.valueOf(caixa.getTotalVendas()));

        float valorRetirada = (float) 0.0;

        for (Retiradas r : rdao.findAll()) {
            if (dataHoje.compareTo(r.getData()) == 0) {
                valorRetirada += r.getValor();
            }
        }

        jTFSaldoFinal.setText(String.valueOf((caixa.getSaldoInicial() +  caixa.getTotalVendas()) - valorRetirada));
        jTFRetiradas.setText(String.valueOf(valorRetirada));
        caixa.setRetiradas(valorRetirada);
        caixa.setSaldoFinal((caixa.getSaldoInicial() +  caixa.getTotalVendas()) - valorRetirada);
        CaixaDAO.getInstance().merge(caixa);
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFTelaCaixaFechar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFTelaCaixaFechar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFTelaCaixaFechar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFTelaCaixaFechar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFTelaCaixaFechar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBFechar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField jTFRetiradas;
    private javax.swing.JTextField jTFSaldoFinal;
    private javax.swing.JTextField jTFSaldoInicial;
    private javax.swing.JTextField jTFTotalVendas;
    // End of variables declaration//GEN-END:variables
}
