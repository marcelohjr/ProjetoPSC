package Apresentação;

import javax.swing.table.DefaultTableModel;
import DataAccess.MercadoriaDAO;
import DomainModel.FContexto;
import DomainModel.Mercadoria;
import javax.swing.JOptionPane;
import javax.swing.table.TableRowSorter;

/**
 * @author rubensjr
 */
public class ListarMercadorias extends javax.swing.JFrame {
    
    public ListarMercadorias() {
        initComponents();
        DefaultTableModel modelo = (DefaultTableModel) jTMercadorias.getModel();
        jTMercadorias.setRowSorter(new TableRowSorter(modelo));
        readJTable();
    }
    
    public void readJTable() {
        DefaultTableModel modelo = (DefaultTableModel) jTMercadorias.getModel();
        modelo.setNumRows(0);
        MercadoriaDAO mdao = new MercadoriaDAO();
        
        for (Mercadoria m : mdao.findAll()) {
            modelo.addRow(new Object[]{
                m.getCodigo(),
                m.getNome(),
                m.getUnidade(),
                m.getEstoqueAtual(),
                m.getPrecoCompra(),
                m.getPrecoVenda()
            });
        }
    }
    
    public void readJTableForDesc(){
        DefaultTableModel modelo = (DefaultTableModel) jTMercadorias.getModel();
        modelo.setNumRows(0);
        MercadoriaDAO mdao = new MercadoriaDAO();
        
        for (Mercadoria m : mdao.findAll()) {
            if(m.getNome().toUpperCase().startsWith(jTFBuscaMercadorias.getText().toUpperCase())){
                modelo.addRow(new Object[]{
                    m.getCodigo(),
                    m.getNome(),
                    m.getUnidade(),
                    m.getEstoqueAtual(),
                    m.getPrecoCompra(),
                    m.getPrecoVenda()
                });
            }    
        }
    }
    
    public void readJTableForId(){
        DefaultTableModel modelo = (DefaultTableModel) jTMercadorias.getModel();
        modelo.setNumRows(0);
        MercadoriaDAO mdao = new MercadoriaDAO();
        
        for (Mercadoria m : mdao.findAll()) {
            if(m.getCodigo() == Integer.parseInt(jTFBuscaMercadorias.getText())){
                modelo.addRow(new Object[]{
                    m.getCodigo(),
                    m.getNome(),
                    m.getUnidade(),
                    m.getEstoqueAtual(),
                    m.getPrecoCompra(),
                    m.getPrecoVenda()
                });
            }    
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTMercadorias = new javax.swing.JTable();
        jBSelecionar = new javax.swing.JButton();
        jBBuscar = new javax.swing.JButton();
        jTFBuscaMercadorias = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jCBTipoPesquisa = new javax.swing.JComboBox<>();
        Fechar = new javax.swing.JButton();
        jBListarTodos = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTMercadorias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Descrição", "Unidade", "Estoque Atual", "Preço Compra", "Preço Venda"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTMercadorias.getTableHeader().setReorderingAllowed(false);
        jTMercadorias.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTMercadoriasMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTMercadorias);
        if (jTMercadorias.getColumnModel().getColumnCount() > 0) {
            jTMercadorias.getColumnModel().getColumn(0).setResizable(false);
            jTMercadorias.getColumnModel().getColumn(0).setPreferredWidth(50);
            jTMercadorias.getColumnModel().getColumn(1).setResizable(false);
            jTMercadorias.getColumnModel().getColumn(1).setPreferredWidth(250);
            jTMercadorias.getColumnModel().getColumn(2).setResizable(false);
            jTMercadorias.getColumnModel().getColumn(2).setPreferredWidth(40);
            jTMercadorias.getColumnModel().getColumn(3).setResizable(false);
            jTMercadorias.getColumnModel().getColumn(3).setPreferredWidth(70);
            jTMercadorias.getColumnModel().getColumn(4).setResizable(false);
            jTMercadorias.getColumnModel().getColumn(4).setPreferredWidth(70);
            jTMercadorias.getColumnModel().getColumn(5).setResizable(false);
            jTMercadorias.getColumnModel().getColumn(5).setPreferredWidth(70);
        }

        jBSelecionar.setText("Selecionar");
        jBSelecionar.setEnabled(false);
        jBSelecionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSelecionarActionPerformed(evt);
            }
        });

        jBBuscar.setText("Buscar");
        jBBuscar.setEnabled(false);
        jBBuscar.setMaximumSize(new java.awt.Dimension(96, 18));
        jBBuscar.setMinimumSize(new java.awt.Dimension(96, 18));
        jBBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarActionPerformed(evt);
            }
        });

        jTFBuscaMercadorias.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFBuscaMercadoriasKeyReleased(evt);
            }
        });

        jLabel1.setText("Pesquisar por:");

        jCBTipoPesquisa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Código", "Nome" }));
        jCBTipoPesquisa.setMaximumSize(new java.awt.Dimension(70, 18));
        jCBTipoPesquisa.setMinimumSize(new java.awt.Dimension(70, 18));

        Fechar.setText("Fechar");
        Fechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FecharActionPerformed(evt);
            }
        });

        jBListarTodos.setText("Listar Todos");
        jBListarTodos.setEnabled(false);
        jBListarTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBListarTodosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCBTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                        .addComponent(jTFBuscaMercadorias, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(jBBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jBListarTodos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jBSelecionar)
                        .addGap(18, 18, 18)
                        .addComponent(Fechar, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCBTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jBBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTFBuscaMercadorias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 297, Short.MAX_VALUE)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBSelecionar)
                    .addComponent(Fechar)
                    .addComponent(jBListarTodos))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarActionPerformed
        if(jCBTipoPesquisa.getSelectedItem().equals("Nome"))
            readJTableForDesc();
        else{
            try{
                readJTableForId();
            }catch(NumberFormatException ex){
                readJTable();
                JOptionPane.showMessageDialog(this, " Código Inválido!");
                jTFBuscaMercadorias.setText("");
            }
        }
    }//GEN-LAST:event_jBBuscarActionPerformed
 
    private void FecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FecharActionPerformed
        dispose();
    }//GEN-LAST:event_FecharActionPerformed

    private void jBSelecionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSelecionarActionPerformed
        if (jTMercadorias.getSelectedRow() != -1) {
            Mercadoria m = new Mercadoria();
            m = MercadoriaDAO.getInstance().getById((int)jTMercadorias.getValueAt(jTMercadorias.getSelectedRow(), 0));
            
            if(FContexto.getMercadoria().isVisible()){
                FContexto.getMercadoria().setMercadoria(m);
                FContexto.getMercadoria().desabilitarCampos();
            }
            
            if(FContexto.getNotaFiscal().isVisible())
                FContexto.getNotaFiscal().setMercadoria(m);
            
            if(FContexto.getVendas().isVisible())
                FContexto.getVendas().setMercadoria(m);
            
            dispose();
        }
    }//GEN-LAST:event_jBSelecionarActionPerformed

    private void jTMercadoriasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTMercadoriasMouseClicked
        if (jTMercadorias.getSelectedRow() != -1) {
            jBSelecionar.setEnabled(true);
        }
    }//GEN-LAST:event_jTMercadoriasMouseClicked

    private void jTFBuscaMercadoriasKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFBuscaMercadoriasKeyReleased
        if(jTFBuscaMercadorias.getText().equals(""))
            jBBuscar.setEnabled(false);
        else 
            jBBuscar.setEnabled(true);
        
        jBListarTodos.setEnabled(true);
    }//GEN-LAST:event_jTFBuscaMercadoriasKeyReleased

    private void jBListarTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBListarTodosActionPerformed
        readJTable();
        jBListarTodos.setEnabled(false);
    }//GEN-LAST:event_jBListarTodosActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ListarMercadorias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ListarMercadorias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ListarMercadorias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ListarMercadorias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fjTUsuarios       /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ListarMercadorias().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Fechar;
    private javax.swing.JButton jBBuscar;
    private javax.swing.JButton jBListarTodos;
    private javax.swing.JButton jBSelecionar;
    private javax.swing.JComboBox<String> jCBTipoPesquisa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTFBuscaMercadorias;
    private javax.swing.JTable jTMercadorias;
    // End of variables declaration//GEN-END:variables
}
