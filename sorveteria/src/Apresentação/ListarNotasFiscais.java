package Apresentação;

import DataAccess.NotaFiscalDAO;
import DomainModel.FContexto;
import DomainModel.NotaFiscal;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class ListarNotasFiscais extends javax.swing.JFrame {

    public ListarNotasFiscais() {
        initComponents();
        DefaultTableModel modelo = (DefaultTableModel) jTNotasFiscais.getModel();
        jTNotasFiscais.setRowSorter(new TableRowSorter(modelo));
        readJTable();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jBListarTodos = new javax.swing.JButton();
        jBSelecionar = new javax.swing.JButton();
        Fechar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTNotasFiscais = new javax.swing.JTable();
        jBBuscar = new javax.swing.JButton();
        jTFBuscaNotasFiscais = new javax.swing.JTextField();
        jCBTipoPesquisa = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jBListarTodos.setText("Listar Todos");
        jBListarTodos.setEnabled(false);
        jBListarTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBListarTodosActionPerformed(evt);
            }
        });

        jBSelecionar.setText("Selecionar");
        jBSelecionar.setEnabled(false);
        jBSelecionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSelecionarActionPerformed(evt);
            }
        });

        Fechar.setText("Fechar");
        Fechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FecharActionPerformed(evt);
            }
        });

        jTNotasFiscais.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Número", "Fornecedor", "Data de Entrega", "Valor Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Long.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTNotasFiscais.getTableHeader().setReorderingAllowed(false);
        jTNotasFiscais.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTNotasFiscaisMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTNotasFiscais);
        if (jTNotasFiscais.getColumnModel().getColumnCount() > 0) {
            jTNotasFiscais.getColumnModel().getColumn(0).setPreferredWidth(50);
            jTNotasFiscais.getColumnModel().getColumn(2).setPreferredWidth(200);
            jTNotasFiscais.getColumnModel().getColumn(3).setPreferredWidth(60);
            jTNotasFiscais.getColumnModel().getColumn(4).setPreferredWidth(60);
        }

        jBBuscar.setText("Buscar");
        jBBuscar.setEnabled(false);
        jBBuscar.setMaximumSize(new java.awt.Dimension(96, 18));
        jBBuscar.setMinimumSize(new java.awt.Dimension(96, 18));
        jBBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarActionPerformed(evt);
            }
        });

        jTFBuscaNotasFiscais.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFBuscaNotasFiscaisKeyReleased(evt);
            }
        });

        jCBTipoPesquisa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Código", "Fornecedor" }));
        jCBTipoPesquisa.setMaximumSize(new java.awt.Dimension(70, 18));
        jCBTipoPesquisa.setMinimumSize(new java.awt.Dimension(70, 18));

        jLabel1.setText("Pesquisar por:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCBTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                        .addComponent(jTFBuscaNotasFiscais, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(jBBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jBListarTodos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jBSelecionar)
                        .addGap(18, 18, 18)
                        .addComponent(Fechar, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCBTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jBBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTFBuscaNotasFiscais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 297, Short.MAX_VALUE)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBSelecionar)
                    .addComponent(Fechar)
                    .addComponent(jBListarTodos))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    public void readJTable() {
        DefaultTableModel modelo = (DefaultTableModel) jTNotasFiscais.getModel();
        modelo.setNumRows(0);
        NotaFiscalDAO nfdao = new NotaFiscalDAO();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dataEntrega;

        for (NotaFiscal nf : nfdao.findAll()) {
            dataEntrega = formatter.format(nf.getDataEntrega());
            
            modelo.addRow(new Object[]{
                nf.getCodigo(),
                nf.getNumero(),
                nf.getDescricaoFornecedor(),
                dataEntrega,
                nf.getValorTotal()
            });
        }
    }

    public void readJTableForDesc() {
        DefaultTableModel modelo = (DefaultTableModel) jTNotasFiscais.getModel();
        modelo.setNumRows(0);
        NotaFiscalDAO nfdao = new NotaFiscalDAO();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dataEntrega;

        for (NotaFiscal nf : nfdao.findAll()) {
            dataEntrega = formatter.format(nf.getDataEntrega());
            if (nf.getDescricaoFornecedor().toUpperCase().startsWith(jTFBuscaNotasFiscais.getText().toUpperCase())) {
                modelo.addRow(new Object[]{
                    nf.getCodigo(),
                    nf.getNumero(),
                    nf.getDescricaoFornecedor(),
                    dataEntrega,
                    nf.getValorTotal()
                });
            }
        }
    }

    public void readJTableForId() {
        DefaultTableModel modelo = (DefaultTableModel) jTNotasFiscais.getModel();
        modelo.setNumRows(0);
        NotaFiscalDAO nfdao = new NotaFiscalDAO();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dataEntrega;

        for (NotaFiscal nf : nfdao.findAll()) {
            dataEntrega = formatter.format(nf.getDataEntrega());
            if (nf.getCodigo() == Integer.parseInt(jTFBuscaNotasFiscais.getText())) {
                modelo.addRow(new Object[]{
                    nf.getCodigo(),
                    nf.getNumero(),
                    nf.getDescricaoFornecedor(),
                    dataEntrega,
                    nf.getValorTotal()
                });
            }
        }
    }

    private void jBListarTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBListarTodosActionPerformed
        readJTable();
        jBListarTodos.setEnabled(false);
    }//GEN-LAST:event_jBListarTodosActionPerformed

    private void jBSelecionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSelecionarActionPerformed
        if (jTNotasFiscais.getSelectedRow() != -1) {
            NotaFiscal n = new NotaFiscal();
            n = NotaFiscalDAO.getInstance().getById((int) jTNotasFiscais.getValueAt(jTNotasFiscais.getSelectedRow(), 0));

            FContexto.getNotaFiscal().setNotaFiscal(n);
            FContexto.getNotaFiscal().desabilitarCampos();

            dispose();
        }
    }//GEN-LAST:event_jBSelecionarActionPerformed

    private void FecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FecharActionPerformed
        dispose();
    }//GEN-LAST:event_FecharActionPerformed

    private void jTNotasFiscaisMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTNotasFiscaisMouseClicked
        if (jTNotasFiscais.getSelectedRow() != -1) {
            jBSelecionar.setEnabled(true);
        }
    }//GEN-LAST:event_jTNotasFiscaisMouseClicked

    private void jBBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarActionPerformed
        if (jCBTipoPesquisa.getSelectedItem().equals("Fornecedor")) {
            readJTableForDesc();
        } else {
            try {
                readJTableForId();
            } catch (NumberFormatException ex) {
                readJTable();
                JOptionPane.showMessageDialog(this, " Código Inválido!");
                jTFBuscaNotasFiscais.setText("");
            }
        }
    }//GEN-LAST:event_jBBuscarActionPerformed

    private void jTFBuscaNotasFiscaisKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFBuscaNotasFiscaisKeyReleased
        if (jTFBuscaNotasFiscais.getText().equals("")) {
            jBBuscar.setEnabled(false);
        } else {
            jBBuscar.setEnabled(true);
        }

        jBListarTodos.setEnabled(true);
    }//GEN-LAST:event_jTFBuscaNotasFiscaisKeyReleased

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ListarNotasFiscais.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ListarNotasFiscais.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ListarNotasFiscais.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ListarNotasFiscais.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ListarNotasFiscais().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Fechar;
    private javax.swing.JButton jBBuscar;
    private javax.swing.JButton jBListarTodos;
    private javax.swing.JButton jBSelecionar;
    private javax.swing.JComboBox<String> jCBTipoPesquisa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTFBuscaNotasFiscais;
    private javax.swing.JTable jTNotasFiscais;
    // End of variables declaration//GEN-END:variables
}
