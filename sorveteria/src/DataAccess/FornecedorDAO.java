package DataAccess;

import DomainModel.Fornecedor;
import java.util.*;
import javax.persistence.*;

public class FornecedorDAO {

    private static FornecedorDAO instance;
    protected EntityManager entityManager;

    public static FornecedorDAO getInstance() {
        if (instance == null) {
            instance = new FornecedorDAO();
        }
        return instance;
    }

    public FornecedorDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("sorveteriaPU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }

    public Fornecedor getById(final int id) {
        return entityManager.find(Fornecedor.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Fornecedor> findAll() {
        return entityManager.createQuery("FROM " + Fornecedor.class.getName() + " f").getResultList();
    }

    public void persist(Fornecedor fornecedor) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(fornecedor);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Fornecedor fornecedor) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(fornecedor);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remove(Fornecedor fornecedor) {
        try {
            entityManager.getTransaction().begin();
            fornecedor = entityManager.find(Fornecedor.class, fornecedor.getCodigo());
            entityManager.remove(fornecedor);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final int id) {
        try {
            Fornecedor fornecedor = getById(id);
            remove(fornecedor);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
