package DataAccess;

import DomainModel.MercadoriaFornecedor;
import java.util.*;
import javax.persistence.*;

public class MercadoriaFornecedorDAO {

    private static MercadoriaFornecedorDAO instance;
    protected EntityManager entityManager;

    public static MercadoriaFornecedorDAO getInstance() {
        if (instance == null) {
            instance = new MercadoriaFornecedorDAO();
        }
        return instance;
    }

    public MercadoriaFornecedorDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("sorveteriaPU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }

    public MercadoriaFornecedor getById(final int id) {
        return entityManager.find(MercadoriaFornecedor.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<MercadoriaFornecedor> findAll() {
        return entityManager.createQuery("FROM " + MercadoriaFornecedor.class.getName() + " mf").getResultList();
    }

    public void persist(MercadoriaFornecedor mercadoriaFornecedor) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(mercadoriaFornecedor);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(MercadoriaFornecedor mercadoriaFornecedor) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(mercadoriaFornecedor);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remove(MercadoriaFornecedor mercadoriaFornecedor) {
        try {
            entityManager.getTransaction().begin();
            mercadoriaFornecedor = entityManager.find(MercadoriaFornecedor.class, mercadoriaFornecedor.getIdMercadoria());
            entityManager.remove(mercadoriaFornecedor);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final int id) {
        try {
            MercadoriaFornecedor mercadoriaFornecedor = getById(id);
            remove(mercadoriaFornecedor);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
