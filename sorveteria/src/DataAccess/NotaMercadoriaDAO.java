package DataAccess;

import DomainModel.NotaMercadoria;
import java.util.*;
import javax.persistence.*;

public class NotaMercadoriaDAO {

    private static NotaMercadoriaDAO instance;
    protected EntityManager entityManager;

    public static NotaMercadoriaDAO getInstance() {
        if (instance == null) {
            instance = new NotaMercadoriaDAO();
        }
        return instance;
    }

    public NotaMercadoriaDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("sorveteriaPU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }

    public NotaMercadoria getById(final int id) {
        return entityManager.find(NotaMercadoria.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<NotaMercadoria> findAll() {
        return entityManager.createQuery("FROM " + NotaMercadoria.class.getName() + " u").getResultList();
    }

    public void persist(NotaMercadoria notaMercadoria) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(notaMercadoria);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(NotaMercadoria notaMercadoria) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(notaMercadoria);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remove(NotaMercadoria notaMercadoria) {
        try {
            entityManager.getTransaction().begin();
            notaMercadoria = entityManager.find(NotaMercadoria.class, notaMercadoria.getIdNota());
            entityManager.remove(notaMercadoria);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final int id) {
        try {
            NotaMercadoria notaMercadoria = getById(id);
            remove(notaMercadoria);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
