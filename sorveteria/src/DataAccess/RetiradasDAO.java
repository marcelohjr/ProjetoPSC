package DataAccess;

import DomainModel.Retiradas;
import java.util.*;
import javax.persistence.*;

public class RetiradasDAO {

    private static RetiradasDAO instance;
    protected EntityManager entityManager;

    public static RetiradasDAO getInstance() {
        if (instance == null) {
            instance = new RetiradasDAO();
        }
        return instance;
    }

    public RetiradasDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("sorveteriaPU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }

    public Retiradas getById(final long id) {
        return entityManager.find(Retiradas.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Retiradas> findAll() {
        return entityManager.createQuery("FROM " + Retiradas.class.getName() + " r").getResultList();
    }

    public void persist(Retiradas retiradas) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(retiradas);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Retiradas retiradas) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(retiradas);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remove(Retiradas retiradas) {
        try {
            entityManager.getTransaction().begin();
            retiradas = entityManager.find(Retiradas.class, retiradas.getCodigo());
            entityManager.remove(retiradas);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final long id) {
        try {
            Retiradas retiradas = getById(id);
            remove(retiradas);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
