package DataAccess;
import DomainModel.NotaFiscal;
import java.util.*;
import javax.persistence.*;

public class NotaFiscalDAO {
private static NotaFiscalDAO instance;
protected EntityManager entityManager;

    public static NotaFiscalDAO getInstance(){
    if (instance == null){
        instance = new NotaFiscalDAO();
    }
    return instance;
    }

    public NotaFiscalDAO(){
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager(){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("sorveteriaPU");
        if(entityManager == null){
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }

    public NotaFiscal getById(final int id){
        return entityManager.find(NotaFiscal.class, id);
    }
   
    @SuppressWarnings("unchecked")
    public List<NotaFiscal> findAll(){
        return entityManager.createQuery("FROM " + NotaFiscal.class.getName()+" n").getResultList();
    }

    public void persist(NotaFiscal notaFiscal){
        try{
            entityManager.getTransaction().begin();
            entityManager.persist(notaFiscal);
            entityManager.getTransaction().commit();
        }catch(Exception ex){
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }  
    }

    public void merge(NotaFiscal notaFiscal){
        try{
            entityManager.getTransaction().begin();
            entityManager.merge(notaFiscal);
            entityManager.getTransaction().commit();
        }catch(Exception ex){
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remove(NotaFiscal notaFiscal){
        try{
            entityManager.getTransaction().begin();
            notaFiscal = entityManager.find(NotaFiscal.class, notaFiscal.getCodigo());
            entityManager.remove(notaFiscal);
            entityManager.getTransaction().commit();
        }catch(Exception ex){
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final int id){
        try{
            NotaFiscal notaFiscal = getById(id);
            remove(notaFiscal);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}

