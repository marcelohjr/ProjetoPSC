package Validações;

/*
 * @author rubensjr
 */
public class validaEmail {
    
    public static boolean isEmail(String email){
        String padrao = "[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})";
        return email.matches(padrao);
    }
    
}
